//
//  Nameable.swift
//  Getpass
//
//  Created by vadim vitvickiy on 10.02.17.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import RxDataSources
import SwiftyUtils
import SwifterSwift

protocol Identifiable: IdentifiableType, Equatable where Identity == Int {
    var id: Int { get set }
    var name: String? { get set }
}

extension Identifiable {
    
    var identity: Int {
        return id
    }
    
    public static func ==(lhs: Self, rhs: Self) -> Bool {
        var isName = false
        if let name = lhs.name {
            isName = name == rhs.name
        }
        return lhs.id == rhs.id || isName
    }
}

