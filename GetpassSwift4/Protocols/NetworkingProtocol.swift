//
//  NetworkableProtocol.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 15/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Moya

protocol NetworkingProtocol: TargetType {

    var url: String {get}

    var headers: [String: String]? {get}
}

extension NetworkingProtocol {
    var baseURL: URL {
        return URL(string: "http://getpass.altuhov-dev.ru:80/api/v1/")!
    }

    var url: String {
        return "\(baseURL)\(path)"
    }

    var headers: [String: String]? {
        var params: [String : String] = [:]

        if let token = token() {
            params["Authorization"] = "Token \(token)"
//            params["SPORT"] = roleStates.sportID.string
//            params["ROLE"] = roleStates.currentRole.rawValue
        }

        return params
    }

    var parameterEncoding: Moya.ParameterEncoding {
        return method == .get ? URLEncoding() : JSONEncoding()
    }

    var sampleData: Data {
        return Data()
    }
}

