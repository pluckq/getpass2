//
//  AppCoordinator.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 11/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift

final class AppCoordinator: BaseCoordinator<Void>, HasDisposeBag {
    
    var window: UIWindow?
    
    init(_ window: UIWindow?) {
        self.window = window
    }
    
    override func start() -> Observable<Void> {
        if let token = keychain.get("token") {
            print(token)
            showMainFlow()
        } else {
            showAuth()
        }
        return Observable.never()
    }
    
    func showAuth() {
        let authCoordinator = AuthCoordinator(window: window,
                                              dependencies: AuthDependency(dataManager: AuthDataManager(),
                                                                           validationManager: AuthValidationManager()))
        coordinate(to: authCoordinator)
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    func showMainFlow() {
        let mainFlowCoordinator = MainFlowCoordinator(window)
        coordinate(to: mainFlowCoordinator)
            .subscribe()
            .disposed(by: disposeBag)
    }
}
