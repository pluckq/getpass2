//
//  AppAppearance.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 08/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit

final class AppAppearance {

    class func setupSideMenu() {
        SideMenuController.preferences.drawing.menuButtonImage = #imageLiteral(resourceName: "menu")
        SideMenuController.preferences.drawing.sidePanelPosition = .underCenterPanelLeft
        SideMenuController.preferences.drawing.sidePanelWidth = appDelegate.window!.frame.width - 50
        SideMenuController.preferences.animating.statusBarBehaviour = .showUnderlay
        SideMenuController.preferences.drawing.centerPanelShadow = false
        SideMenuController.preferences.interaction.panningEnabled = true
        SideMenuController.preferences.interaction.swipingEnabled = true
        SideMenuController.preferences.animating.transitionAnimator = nil
    }
}
