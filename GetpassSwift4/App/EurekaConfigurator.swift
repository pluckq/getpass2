//
//  EurekaConfigurator.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 08/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit
import Eureka

extension FormViewController {
    
    func setupVC() {
        view.tintColor = UIColor.AppColor.Gray.Dark
        tableView?.backgroundColor = .white
        tableView?.separatorStyle = .none
        tableView?.tintColor = UIColor.AppColor.Gray.Dark
        tableView?.estimatedSectionHeaderHeight = 35
        tableView?.sectionFooterHeight = 0
        tableView?.tableFooterView = nil
        tableView.removeTableFooterView()
    }
    
    func setupRows() {
        
        ButtonRow.defaultCellUpdate = { [weak self] cell, row in
            self?.setupCell(cell: cell)
        }
        
        DateInlineRow.defaultCellUpdate = { [weak self] cell, row in
            self?.setupCell(cell: cell)
            cell.accessoryType = .disclosureIndicator
        }
        
        DateTimeInlineRow.defaultCellUpdate = { [weak self] cell, row in
            self?.setupCell(cell: cell)
            cell.accessoryType = .disclosureIndicator
        }
        
        PushRow<String>.defaultCellUpdate = { [weak self] cell, row in
            self?.setupCell(cell: cell)
        }
        
        SuggestionAccessoryRow<City>.defaultCellUpdate = { [weak self] cell, row in
            self?.setupCell(cell: cell)
            cell.textField.textColor = UIColor.AppColor.Gray.Dark
            cell.textField.textAlignment = .right
        }
        
        PickerInlineRow<String>.defaultCellUpdate = { [weak self] cell, row in
            self?.setupCell(cell: cell)
            cell.accessoryType = .disclosureIndicator
        }
        
        PickerInlineRow<Gender>.defaultCellUpdate = { [weak self] cell, row in
            self?.setupCell(cell: cell)
            cell.accessoryType = .disclosureIndicator
        }
        
        TextRow.defaultCellUpdate = { [weak self] cell, row in
            self?.setupCell(cell: cell)
            cell.textField.textColor = UIColor.AppColor.Gray.Dark
        }
        
        PhoneRow.defaultCellUpdate = { [weak self] cell, row in
            self?.setupCell(cell: cell)
        }
        
        EmailRow.defaultCellUpdate = { [weak self] cell, row in
            self?.setupCell(cell: cell)
        }
        
        MultipleSelectorRow<String>.defaultCellUpdate = { [weak self] cell, row in
            self?.setupCell(cell: cell)
        }
        
        IntRow.defaultCellUpdate = { [weak self] cell, row in
            self?.setupCell(cell: cell)
            cell.textField.textColor = UIColor.AppColor.Gray.Dark
        }
        
        URLRow.defaultCellUpdate = { [weak self] cell, row in
            self?.setupCell(cell: cell)
            cell.textField.textColor = UIColor.AppColor.Gray.Dark
        }
        
        SwitchRow.defaultCellUpdate = { [weak self] cell, row in
            self?.setupCell(cell: cell)
            cell.switchControl.onTintColor = UIColor.AppColor.Orange
        }
        
        TextAreaRow.defaultCellUpdate = { cell, row in
            cell.isUserInteractionEnabled = true
            cell.backgroundColor = UIColor.AppColor.Gray.Light
            cell.layoutMargins.left = eurekaMargin
            cell.layoutMargins.right = eurekaMargin
            cell.textView.textColor = UIColor.AppColor.Gray.Dark
            cell.textView.backgroundColor = UIColor.AppColor.Gray.Light
            row.textAreaHeight = .dynamic(initialTextViewHeight: 15)
        }
        
        SegmentedRow<String>.defaultCellUpdate = { [weak self] cell, row in
            self?.setupCell(cell: cell)
            cell.segmentedControl.borderColor = UIColor.AppColor.Gray.Dark
        }
        
        SliderRow.defaultCellUpdate = { cell, row in
            cell.backgroundColor = UIColor.AppColor.Gray.Light
        }
    }
    
    func setupCell(cell: BaseCell) {
        cell.backgroundColor = UIColor.AppColor.Gray.Light
        cell.height = {eurekaCellHeight}
        cell.layoutMargins = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        cell.layoutMargins.left = eurekaMargin
        cell.layoutMargins.right = eurekaMargin
        cell.preservesSuperviewLayoutMargins = true
        cell.contentView.preservesSuperviewLayoutMargins = true
        cell.detailTextLabel?.textColor = UIColor.AppColor.Gray.Dark
        cell.textLabel?.font = cell.textLabel?.font.withSize(17)
        cell.detailTextLabel?.font = cell.detailTextLabel?.font.withSize(17)
        cell.textLabel?.textColor = .black
    }
}

