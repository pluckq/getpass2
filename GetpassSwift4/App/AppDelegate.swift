//
//  AppDelegate.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 30/10/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    private var appCoordinator: AppCoordinator!
    private let bag = DisposeBag()
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        setup()
        return true
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        setupFlow()
        return true
    }
    
    func setup() {
        AppAppearance.setupSideMenu()
        appCoordinator = AppCoordinator(window)
//        Fabric.with([Crashlytics.self])
    }
    
    func setupFlow() {
//        appCoordinator.start()
//            .subscribe()
//            .disposed(by: bag)
        let nc = UINavigationController()
        window?.rootViewController = nc
        window?.makeKeyAndVisible()
        let c = UserInfoCoordinator(nc: nc)
        c.start()
            .subscribe()
            .disposed(by: bag)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey: Any]) -> Bool {
        return Simplicity.application(app, open: url, options: options)
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return Simplicity.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
}

