//
//  Network.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 16/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Moya
import RxSwift

struct Network<T: NetworkingProtocol>: HasDisposeBag {
    
    fileprivate var provider = MoyaProvider<T>(endpointClosure: {
        target -> Endpoint<T> in
        return Endpoint<T>(
            url: target.url,
            sampleResponseClosure: {.networkResponse(200, target.sampleData)},
            method: target.method,
            task: target.task,
            httpHeaderFields: target.headers)})
    
    func request(_ target: T) -> Observable<Response> {
        printindebug(target.method, target.headers ?? [], target.url, target.task)
        return provider.rx.request(target).asObservable().trackActivity(activity).rxServerError().filterSuccessfulStatusCodes()
    }
}
