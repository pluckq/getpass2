//
//  Constants.swift
//  Getpass
//
//  Created by vadim vitvickiy on 08.12.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import KeychainSwift
import SwiftyUtils
import SwifterSwift

let keychain = KeychainSwift()
let appDelegate = UIApplication.shared.delegate as! AppDelegate

let eurekaCellHeight: CGFloat = 56
let eurekaMargin: CGFloat = 15
let eurekaHeaderHeight = 20
let eurekaFooterHeight = 0

func mainWindow() -> UIViewController? {
    return appDelegate.window?.rootViewController
}

func token() -> String? {
    return keychain.get("token")
}

func printindebug(_ items: Any...) {
    #if DEBUG
        print(items)
    #endif
}

