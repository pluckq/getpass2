//
//  MainFlowCoordinator.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 19/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit

protocol HasMainFlowCoordinator {
    var coordinator: MainFlowCoordinator { get set }
}

class MainFlowCoordinator: BaseCoordinator<Void> {
    
    private var window: UIWindow?
    private var sideMenuController: SideMenuController!
    
    init(_ window: UIWindow?) {
        self.window = window
    }
    
    func showProfile() {
        
    }
    
    func showTeams() {
        
    }
    
    func showFriendlyGames() {
        
    }
    
    func showTrainings() {
        
    }
    
    func showFriends() {
        
    }
    
    func showMessages() {
        
    }
    
    func showTournaments() {
        
    }
    
    func showOpenGames() {
        
    }
    
    func showBookmarks() {
        
    }
    
    func showSettings() {
        
    }
    
//    enum Page {
//        case profile
//        case teams
//        case friendlyGames
//        case trainings
//        case friends
//        case messages
//        case tournaments
//        case openGames
//        case bookmarks
//        case settings
//    }
    
//    private var page: Page = .profile
    
    
//    override func start(with completion: @escaping () -> Void = {}) {
//        super.start(with: completion)
//        sideMenuController = SideMenuController()
//        let sideMenu = LeftMenuVC.storyboardInstance()
//        sideMenu.dependencies = SideMenuDependencies(coordinator: self, dataManager: SideMenuDataManager())
//        sideMenuController.delegate = sideMenu
//        sideMenuController.embed(sideViewController: sideMenu)
//        root(sideMenuController)
//        setupActivePage(page)
//    }
//    
//    override func stop(with completion: @escaping () -> Void) {
//        rootViewController.dismiss(animated: true) {
//            completion()
//        }
//    }
//    
//    func display(page: Page) {
//        setupActivePage(page)
//    }
//
//    private func setupActivePage(_ page: Page) {
//        switch page {
//        case .profile:
//            let profile = UINavigationController(rootViewController: ProfileVC.storyboardInstance())
//            sideMenuController.embed(centerViewController: profile)
//        case .teams:
//            break
//        case .friendlyGames:
//            break
//        case .trainings:
//            break
//        case .friends:
//            break
//        case .messages:
//            break
//        case .tournaments:
//            break
//        case .openGames:
//            break
//        case .bookmarks:
//            break
//        case .settings:
//            break
//        }
//    }
    
//    func setVC(vc: UIViewController, cacheID: String) {
//        if let controller = sideMenuController.viewController(forCacheIdentifier: cacheID) {
//            sideMenuController.embed(centerViewController: controller)
//        } else {
//            sideMenuController?.embed(centerViewController: vc, cacheIdentifier: cacheID)
//        }
//    }
}
