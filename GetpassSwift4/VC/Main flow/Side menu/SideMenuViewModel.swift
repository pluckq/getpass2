//
//  SideMenuViewModel.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 21/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import RxSwift
import RxCocoa

class SideMenuViewModel: HasDisposeBag {
    
    let sidemenuDidReveal = PublishSubject<Void>()
    
    init(inputs: (itemSelected: Driver<IndexPath>, avatar: Binder<UIImage?>, name: Binder<String?>, role: Binder<String?>), dependencies: SideMenuDependencies) {
        let coordinator = dependencies.coordinator
        
        let itemSelected = inputs.itemSelected
        
        itemSelected.drive(onNext: { ip in
            switch ip.row {
            case 0:
                coordinator.showProfile()
            case 1:
                coordinator.showTeams()
            case 2:
                coordinator.showFriendlyGames()
            case 3:
                coordinator.showTrainings()
            case 4:
                coordinator.showFriends()
            case 5:
                coordinator.showMessages()
            case 6:
                coordinator.showTournaments()
            case 7:
                coordinator.showOpenGames()
            case 8:
                coordinator.showBookmarks()
            case 9:
                coordinator.showSettings()
            default:
                break
            }
        }).disposed(by: disposeBag)
    }
}
