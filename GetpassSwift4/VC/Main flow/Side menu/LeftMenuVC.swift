//
//  LetMenuVC.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 30/10/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LeftMenuVC: UITableViewController, SideMenuControllerDelegate {
    
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var txtRole: UILabel!
    
    private var viewModel: SideMenuViewModel!
    
    var dependencies: SideMenuDependencies!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = SideMenuViewModel(inputs:
            (itemSelected: tableView.rx.itemSelected.asDriver(),
             avatar: imgAvatar.rx.image,
             name: txtName.rx.text,
             role: txtRole.rx.text),
                                      dependencies: dependencies)
    }
    
    func sideMenuControllerDidReveal(_ sideMenuController: SideMenuController) {
        viewModel.sidemenuDidReveal.onNext(())
    }
    
    func sideMenuControllerDidHide(_ sideMenuController: SideMenuController) {
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)!
        tableView.visibleCells.forEach{$0.backgroundColor = UIColor.AppColor.Gray.Light}
        cell.backgroundColor = UIColor.AppColor.selectedColor
    }
}
