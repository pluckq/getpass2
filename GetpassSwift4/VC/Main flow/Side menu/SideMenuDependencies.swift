//
//  SideMenuDependencies.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 21/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit

protocol HasSideMenuDataManager {
    var dataManager: SideMenuDataManager { get set }
}

struct SideMenuDependencies: HasSideMenuDataManager, HasMainFlowCoordinator {
    var coordinator: MainFlowCoordinator
    var dataManager: SideMenuDataManager
}
