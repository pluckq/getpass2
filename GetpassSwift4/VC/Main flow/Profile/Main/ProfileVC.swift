//
//  ProfileVC.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 11/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    
    typealias Dependencies = HasUser

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    deinit {
        print(className)
    }
}
