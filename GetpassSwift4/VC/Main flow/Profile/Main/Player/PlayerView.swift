//
//  PlayerView.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 17/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PlayerView: UIView {
    
    var viewModel: PlayerViewInterface? {
        didSet {
            binds()
        }
    }
    
    func binds() {
        
    }
}

protocol PlayerViewInterface {
    
    var userInfo: Driver<User> { get set }
    
}
