//
//  RegisterNewPassVC.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit

class RegisterNewPassVC: UIViewController {
    
    lazy var mainView: RegisterNewPassView = { [unowned self] in return view as! RegisterNewPassView }()
    var viewModel: RegisterNewPassVM!

    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    func configure() {
        mainView.viewModel = viewModel
    }
    
    deinit {
        print(className)
    }
}
