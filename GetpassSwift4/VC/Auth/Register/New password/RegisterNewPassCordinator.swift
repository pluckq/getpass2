//
//  RegisterNewPassCordinator.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 03/01/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import RxSwift
import RxCocoa

class RegisterNewPassCordinator: BaseCoordinator<Void> {

    private var nc: UINavigationController!
    private var dependencies: AuthDependency!
    private var phoneEmail: PhoneEmail!
    
    init(nc: UINavigationController, dependencies: AuthDependency, phoneEmail: PhoneEmail) {
        self.nc = nc
        self.dependencies = dependencies
        self.phoneEmail = phoneEmail
    }
    
    override func start() -> Observable<Void> {
        let viewModel = RegisterNewPassVM(phoneEmail: phoneEmail, dependencies: dependencies)
        let vc = RegisterNewPassVC.storyboardInstance()
        vc.viewModel = viewModel
        nc.pushViewController(vc)
        
        viewModel.didSendRequest.drive(onNext: { [weak self] result in
            switch result {
            case .success:
                self?.nc.popToRootViewController(animated: true)
            default: break
            }
        }).disposed(by: disposeBag)
        
        return Observable.never()
    }
}
