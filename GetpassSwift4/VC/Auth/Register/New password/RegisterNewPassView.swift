//
//  RegisterNewPassView.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 14/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit

class RegisterNewPassView: UIView {

    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPasswordRepeat: SkyFloatingLabelTextField!
    @IBOutlet weak var btnSend: UIButton!
    
    weak var viewModel: RegisterNewPassVM! {
        didSet {
            binds()
        }
    }
    
    func binds() {
        txtPassword.rx.text.orEmpty.bind(to: viewModel.password).disposed(by: rx.disposeBag)
        txtPasswordRepeat.rx.text.orEmpty.bind(to: viewModel.passwordRepeat).disposed(by: rx.disposeBag)
        btnSend.rx.tap.bind(to: viewModel.sendTap).disposed(by: rx.disposeBag)
        
        viewModel.signupEnabled
            .drive(onNext: { [weak self] valid  in
                self?.btnSend.isEnabled = valid
                self?.btnSend.alpha = valid ? 1.0 : 0.5
            }).disposed(by: disposeBag)
    }
}
