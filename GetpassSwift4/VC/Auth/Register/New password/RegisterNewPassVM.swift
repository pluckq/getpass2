//
//  RegisterNewPassVM.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 14/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import RxSwift
import RxCocoa

class RegisterNewPassVM {
    
    typealias Dependencies = HasAuthDataManager & HasAuthValidationManager
    
    // MARK: Inputs
    
    let password: AnyObserver<String>
    let passwordRepeat: AnyObserver<String>
    let sendTap: AnyObserver<Void>
    
    // MARK: Outputs
    
    let signupEnabled: Driver<Bool>
    let didSendRequest: Driver<AuthResult>
    
    private let signingIn = ActivityIndicator()
    
    init(phoneEmail: PhoneEmail, dependencies: Dependencies) {
        
        let dataManager = dependencies.dataManager
        let validationService = dependencies.validationManager
        
        let _password = BehaviorSubject<String>(value: "")
        password = _password.asObserver()
        
        let _passwordRepeat = BehaviorSubject<String>(value: "")
        passwordRepeat = _passwordRepeat.asObserver()
        
        let validatedPassword = _password
            .map { return validationService.validatePassword($0) }
        
        let validateRepeatPassword = Observable.combineLatest(_password, _passwordRepeat)
            .map { return validationService.validateRepeatedPassword($0, repeatedPassword: $1) }
        
        let _signingIn = signingIn.asObservable()
        
        let _signupEnabled = Observable
            .combineLatest(validatedPassword,
                           validateRepeatPassword,
                           _signingIn)
            { password, repeatPassword, signingIn in
                    password.isValid &&
                    repeatPassword.isValid &&
                    !signingIn
            }.distinctUntilChanged()
        
        signupEnabled = _signupEnabled.asDriver(onErrorJustReturn: false)
        
        let _sendTap = PublishSubject<Void>()
        sendTap = _sendTap.asObserver()
        didSendRequest = _sendTap.withLatestFrom(_password)
            .flatMapLatest { [unowned signingIn] password -> Observable<AuthResult> in
                return dataManager.changePassword(phoneEmail: phoneEmail, password: password)
                    .trackActivity(signingIn)
            }.asDriver(onErrorJustReturn: .error)
    }
}
