//
//  RegisterForgotPassView.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 14/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit

class RegisterForgotPassView: UIView {

    @IBOutlet weak var txtPhoneEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var btnSend: UIButton!
    
    weak var viewModel: RegisterForgotPassVM! {
        didSet {
            binds()
        }
    }
    
    func binds() {
        txtPhoneEmail.rx.text.orEmpty.bind(to: viewModel.phoneEmail).disposed(by: rx.disposeBag)
        btnSend.rx.tap.bind(to: viewModel.sendTap).disposed(by: rx.disposeBag)
        
        viewModel.signupEnabled
            .drive(onNext: { [weak self] valid  in
                self?.btnSend.isEnabled = valid
                self?.btnSend.alpha = valid ? 1.0 : 0.5
            }).disposed(by: disposeBag)
    }
}
