//
//  RegisterForgotPassCoordinator.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 03/01/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import RxSwift

class RegisterForgotPassCoordinator: BaseCoordinator<Void> {
    
    var nc: UINavigationController!
    var dependencies: AuthDependency!
    
    init(nc: UINavigationController, dependencies: AuthDependency) {
        self.nc = nc
        self.dependencies = dependencies
    }
    
    override func start() -> Observable<Void> {
        let viewModel = RegisterForgotPassVM(dependencies: dependencies)
        let vc = RegisterForgotPassVC.storyboardInstance()
        vc.viewModel = viewModel
        nc.pushViewController(vc)
        
        viewModel.didSendRequest.drive(onNext: { [weak self] result in
            switch result {
            case .phoneEmail(let phoneEmail):
                self?.showCode(phoneEmail: phoneEmail)
            default: break
            }
        }).disposed(by: disposeBag)
        
        return Observable.never()
    }
    
    func showCode(phoneEmail: PhoneEmail) {
        let c = RegisterForgotPassCodeCoordinator(nc: nc, dependencies: dependencies, phoneEmail: phoneEmail)
        coordinate(to: c)
            .subscribe()
            .disposed(by: disposeBag)
    }
}
