//
//  RegisterForgotPassVM.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 14/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import RxSwift
import RxCocoa

class RegisterForgotPassVM {
    
    typealias Dependencies = HasAuthDataManager & HasAuthValidationManager

    // MARK: Inputs
    
    let phoneEmail: AnyObserver<String>
    let sendTap: AnyObserver<Void>
    
    // MARK: Outputs
    
    let signupEnabled: Driver<Bool>
    let didSendRequest: Driver<AuthResult>
    
    private let signingIn = ActivityIndicator()
    
    init(dependencies: Dependencies) {
        
        let dataManager = dependencies.dataManager
        let validationService = dependencies.validationManager
        
        let _phoneEmail = BehaviorSubject<String>(value: "")
        phoneEmail = _phoneEmail.asObserver()
        
        let validatedUsername = _phoneEmail
            .map { return validationService.validateUsername($0) }
        
        let userNameType = _phoneEmail
            .map { return validationService.isEmailOrPhone($0) }
        
        let username = Observable.combineLatest(_phoneEmail, userNameType).map { return PhoneEmail(value: $0, type: $1) }
        
        let _signingIn = signingIn.asObservable()
        
        let _signupEnabled = Observable
            .combineLatest(validatedUsername,
                           _signingIn) {
                            username, signingIn in
                            username.isValid &&
                                !signingIn
            }.distinctUntilChanged()
        
        signupEnabled = _signupEnabled.asDriver(onErrorJustReturn: false)
        
        let _sendTap = PublishSubject<Void>()
        sendTap = _sendTap.asObserver()
        didSendRequest = _sendTap.withLatestFrom(username)
            .flatMapLatest { [unowned signingIn] params -> Observable<AuthResult> in
                return dataManager.recoveryPass(phoneEmail: params)
                    .trackActivity(signingIn)
            }.asDriver(onErrorJustReturn: .error)
    }
}
