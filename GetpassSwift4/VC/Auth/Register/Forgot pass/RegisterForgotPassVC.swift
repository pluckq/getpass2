//
//  RegisterForgotPassVC.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit

class RegisterForgotPassVC: UIViewController {
    
    lazy var mainView: RegisterForgotPassView = { [unowned self] in return view as! RegisterForgotPassView }()
    var viewModel: RegisterForgotPassVM!

    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    func configure() {
        mainView.viewModel = viewModel
    }
    
    deinit {
        print(className)
    }
}
