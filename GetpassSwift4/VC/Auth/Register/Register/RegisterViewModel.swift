
//
//  RegisterViewModel.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 14/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RegisterViewModel: HasDisposeBag {

    typealias Dependencies = HasAuthDataManager & HasAuthValidationManager
    
    // MARK: Inputs
    
    let phoneEmail: AnyObserver<String>
    let password: AnyObserver<String>
    let passwordRepeat: AnyObserver<String>
    let registerTap: AnyObserver<Void>
    
    // MARK: Outputs
    
    let signupEnabled: Driver<Bool>
    let showAcceptCode: Driver<AuthResult>
    
    private let signingIn = ActivityIndicator()
    
    init(dependencies: Dependencies) {
        
        let dataManager = dependencies.dataManager
        let validationService = dependencies.validationManager
        
        let _phoneEmail = BehaviorSubject<String>(value: "")
        phoneEmail = _phoneEmail.asObserver()
        
        let validatedUsername = _phoneEmail
            .map { return validationService.validateUsername($0) }
        
        let userNameType = _phoneEmail
            .map { return validationService.isEmailOrPhone($0) }
        
        let _password = BehaviorSubject<String>(value: "")
        password = _password.asObserver()
        
        let _passwordRepeat = BehaviorSubject<String>(value: "")
        passwordRepeat = _passwordRepeat.asObserver()
        
        let validatedPassword = _password
            .map { return validationService.validatePassword($0) }
     
        let validateRepeatPassword = Observable.combineLatest(_password, _passwordRepeat)
            .map { return validationService.validateRepeatedPassword($0, repeatedPassword: $1) }
        
        let usernameAndPassword = Observable.combineLatest(_phoneEmail, userNameType, _password) {
            (phoneEmail: PhoneEmail(value: $0, type: $1), password: $2) }
        
        let _signingIn = signingIn.asObservable()
        
        let _signupEnabled = Observable
            .combineLatest(validatedUsername,
                           validatedPassword,
                           validateRepeatPassword,
                           _signingIn)
            { username, password, repeatPassword, signingIn in
                    username.isValid &&
                    password.isValid &&
                    repeatPassword.isValid &&
                    !signingIn
            }.distinctUntilChanged()
        
        signupEnabled = _signupEnabled.asDriver(onErrorJustReturn: false)
        
        let _registerTap = PublishSubject<Void>()
        registerTap = _registerTap.asObserver()
        showAcceptCode = _registerTap.withLatestFrom(usernameAndPassword)
            .flatMapLatest { [unowned signingIn] params -> Observable<AuthResult> in
                return dataManager.registerUser(phoneEmail: params.phoneEmail, password: params.password)
                    .trackActivity(signingIn)
        }.asDriver(onErrorJustReturn: .error)
    }
}
