//
//  RegisterVC.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController {

    lazy var mainView: RegisterView = { [unowned self] in return view as! RegisterView }()
    var viewModel: RegisterViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    func configure() {
        mainView.viewModel = viewModel
    }
    
    deinit {
        print(className)
    }
}
