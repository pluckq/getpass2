//
//  RegisterView.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 14/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit

class RegisterView: UIView {

    @IBOutlet weak var txtPhoneEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtRepeatPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var btnRegister: UIButton!
    
    weak var viewModel: RegisterViewModel! {
        didSet {
            binds()
        }
    }
    
    func binds() {
        txtPhoneEmail.rx.text.orEmpty.bind(to: viewModel.phoneEmail).disposed(by: rx.disposeBag)
        txtPassword.rx.text.orEmpty.bind(to: viewModel.password).disposed(by: rx.disposeBag)
        txtRepeatPassword.rx.text.orEmpty.bind(to: viewModel.passwordRepeat).disposed(by: rx.disposeBag)
        btnRegister.rx.tap.bind(to: viewModel.registerTap).disposed(by: rx.disposeBag)
        
        viewModel.signupEnabled
            .drive(onNext: { [weak self] valid  in
                self?.btnRegister.isEnabled = valid
                self?.btnRegister.alpha = valid ? 1.0 : 0.5
            }).disposed(by: disposeBag)
    }
}
