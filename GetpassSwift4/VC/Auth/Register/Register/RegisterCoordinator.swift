//
//  RegisterCoordinator.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 23/12/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift

class RegisterCoordinator: BaseCoordinator<Void> {

    var nc: UINavigationController!
    var dependencies: AuthDependency!
    
    init(nc: UINavigationController, dependencies: AuthDependency) {
        self.nc = nc
        self.dependencies = dependencies
    }
    
    override func start() -> Observable<Void> {
        let viewModel = RegisterViewModel(dependencies: dependencies)
        let vc = RegisterVC.storyboardInstance()
        vc.viewModel = viewModel
        nc.pushViewController(vc)
        
        viewModel.showAcceptCode
            .drive(onNext: { [weak self] result in
                switch result {
                case .phoneEmail(let phoneEmail):
                    self?.showAcceptCode(phoneEmail: phoneEmail)
                default:
                    break
                }
        }).disposed(by: disposeBag)
        
        return Observable.never()
    }
    
    func showAcceptCode(phoneEmail: PhoneEmail) {
        let c = RegisterAcceptCodeCoordinator(nc: nc, dependencies: dependencies, phoneEmail: phoneEmail)
        coordinate(to: c)
            .subscribe()
            .disposed(by: disposeBag)
    }
}
