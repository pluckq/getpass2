//
//  RegistetAcceptCodeView.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 14/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit

class RegisterAcceptCodeView: UIView {

    @IBOutlet weak var txtCode: SkyFloatingLabelTextField!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var btnSendAgain: UIButton!
    
    weak var viewModel: RegisterAcceptCodeVM! {
        didSet {
            binds()
        }
    }
    
    func binds() {
        txtCode.rx.text.orEmpty.bind(to: viewModel.code).disposed(by: rx.disposeBag)
        btnSend.rx.tap.bind(to: viewModel.sendCode).disposed(by: rx.disposeBag)
        btnSendAgain.rx.tap.bind(to: viewModel.resendCode).disposed(by: rx.disposeBag)
        
        viewModel.signupEnabled
            .drive(onNext: { [weak self] valid  in
                self?.btnSend.isEnabled = valid
                self?.btnSend.alpha = valid ? 1.0 : 0.5
            }).disposed(by: disposeBag)
    }
}
