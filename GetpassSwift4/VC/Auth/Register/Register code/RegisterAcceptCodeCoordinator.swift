//
//  RegisterCodeCoordinator.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 23/12/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift

enum RegisterAcceptCodeResult {
    case success(phoneEmail: String)
    case error
}

class RegisterAcceptCodeCoordinator: BaseCoordinator<Void>, HasDisposeBag {
    
    private var nc: UINavigationController?
    private var dependencies: AuthDependency!
    private var phoneEmail: PhoneEmail!
    
    init(nc: UINavigationController, dependencies: AuthDependency, phoneEmail: PhoneEmail) {
        self.nc = nc
        self.dependencies = dependencies
        self.phoneEmail = phoneEmail
    }

    override func start() -> Observable<Void> {
        let vc = RegisterAcceptCodeVC.storyboardInstance()
        let viewModel = RegisterAcceptCodeVM(phoneEmail: phoneEmail, dependencies: dependencies)
        vc.viewModel = viewModel
        nc?.pushViewController(vc)
        
        viewModel.didSendCode.drive(onNext: { [weak self] result in
            switch result {
            case .ok(let token):
                token.saveToken()
                self?.showUserInfo()
            default:
                break
            }
        }).disposed(by: disposeBag)
        
        return Observable.never()
    }
    
    func showUserInfo() {
        
    }
}
