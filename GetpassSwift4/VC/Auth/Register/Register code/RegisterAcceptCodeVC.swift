//
//  RegisterAcceptCodeVC.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit

class RegisterAcceptCodeVC: UIViewController {
    
    lazy var mainView: RegisterAcceptCodeView = { [unowned self] in return view as! RegisterAcceptCodeView }()
    var viewModel: RegisterAcceptCodeVM!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    func configure() {
        mainView.viewModel = viewModel
    }
    
    deinit {
        print(className)
    }
}
