//
//  RegisterForgotPassCodeVM.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 14/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import RxSwift
import RxCocoa

class RegisterForgotPassCodeVM: HasDisposeBag {

    typealias Dependencies = HasAuthDataManager & HasAuthValidationManager
    
    // MARK: Inputs
    
    let code: AnyObserver<String>
    let sendCode: AnyObserver<Void>
    let resendCode: AnyObserver<Void>
    
    // MARK: Outputs
    
    let signupEnabled: Driver<Bool>
    let didSendCode: Driver<AuthResult>
    
    private let signingIn = ActivityIndicator()
    
    init(phoneEmail: PhoneEmail, dependencies: Dependencies) {
        
        let dataManager = dependencies.dataManager
        let validationService = dependencies.validationManager
        
        let _code = BehaviorSubject<String>(value: "")
        code = _code.asObserver()
        
        let validateCode = _code.map { validationService.validateCode($0) }
        
        let _signingIn = signingIn.asObservable()
        
        let _signupEnabled = Observable
            .combineLatest(validateCode,
                           _signingIn)
            { validateCode, signingIn in
                validateCode.isValid &&
                    !signingIn
            }.distinctUntilChanged()
        
        signupEnabled = _signupEnabled.asDriver(onErrorJustReturn: false)
        
        let _sendCode = PublishSubject<Void>()
        sendCode = _sendCode.asObserver()
        didSendCode = _sendCode.withLatestFrom(_code)
            .flatMapLatest { [unowned signingIn] code -> Observable<AuthResult> in
                return dataManager.recoveryPassCode(phoneEmail: phoneEmail, code: code)
                    .trackActivity(signingIn)
            }.asDriver(onErrorJustReturn: .error)
        
        let _resendCode = PublishSubject<Void>()
        resendCode = _resendCode.asObserver()
        
        _resendCode.map { dataManager.resendCode(phoneEmail: phoneEmail) }
            .subscribe()
            .disposed(by: disposeBag)
    }
}
