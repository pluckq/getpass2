//
//  RegisterForgotPassCodeCoordinator.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 03/01/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import RxSwift

class RegisterForgotPassCodeCoordinator: BaseCoordinator<Void> {

    private var nc: UINavigationController!
    private var dependencies: AuthDependency!
    private var phoneEmail: PhoneEmail!
    
    init(nc: UINavigationController, dependencies: AuthDependency, phoneEmail: PhoneEmail) {
        self.nc = nc
        self.dependencies = dependencies
        self.phoneEmail = phoneEmail
    }
    
    override func start() -> Observable<Void> {
        let viewModel = RegisterForgotPassCodeVM(phoneEmail: phoneEmail, dependencies: dependencies)
        let vc = RegisterForgotPassCodeVC.storyboardInstance()
        vc.viewModel = viewModel
        nc.pushViewController(vc)
        
        viewModel.didSendCode.drive(onNext: { [weak self] result in
            switch result {
            case .phoneEmail(let phoneEmail):
                self?.showNewPass(phoneEmail: phoneEmail)
            default: break
            }
        }).disposed(by: disposeBag)
        
        return Observable.never()
    }
    
    func showNewPass(phoneEmail: PhoneEmail) {
        let c = RegisterNewPassCordinator(nc: nc, dependencies: dependencies, phoneEmail: phoneEmail)
        coordinate(to: c)
            .subscribe()
            .disposed(by: disposeBag)
    }
}
