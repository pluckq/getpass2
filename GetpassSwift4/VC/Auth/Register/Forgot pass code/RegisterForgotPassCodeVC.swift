//
//  RegisterForgotPassCodeVC.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit

class RegisterForgotPassCodeVC: UIViewController {
    
    lazy var mainView: RegisterForgotPassCodeView = { [unowned self] in return view as! RegisterForgotPassCodeView }()
    var viewModel: RegisterForgotPassCodeVM!

    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    func configure() {
        mainView.viewModel = viewModel
    }
    
    deinit {
        print(className)
    }
}
