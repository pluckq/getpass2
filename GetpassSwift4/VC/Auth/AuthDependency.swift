//
//  AuthDependency.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 10/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit

protocol HasAuthDataManager {
    var dataManager: AuthDataManager { get }
}

protocol HasAuthValidationManager {
    var validationManager: AuthValidationService { get }
}

struct AuthDependency: HasAuthDataManager, HasAuthValidationManager {
    let dataManager: AuthDataManager
    let validationManager: AuthValidationService
}
