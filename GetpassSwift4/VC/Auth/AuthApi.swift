//
//  AuthNetworkManager.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 15/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Moya
import RxSwift

enum AuthApi {
    case registerEmail(email: String, password: String)
    case acceptEmail(email: String, code: String)
    case acceptEmailResendCode(email: String)
    
    case registerPhone(phone: String, password: String)
    case acceptPhone(phone: String, code: String)
    case acceptPhoneResendCode(phone: String)
    
    case loginEmail(email: String, password: String)
    case loginPhone(phone: String, password: String)
    
    case loginFB(token: String)
    case loginVK(token: String)
    
    case recoverypassEmail(email: String)
    case recoverypassEmailAccept(email: String, code: String)
    case recoverypassEmailChangePassword(email: String, password: String)
    
    case recoverypassPhone(phone: String)
    case recoverypassPhoneAccept(phone: String, code: String)
    case recoverypassPhoneChangePassword(phone: String, password: String)
}

extension AuthApi: NetworkingProtocol {
    
    var path: String {
        switch self {
        case .registerEmail:
            return "registration/email/"
        case .registerPhone:
            return "registration/phone/"
        case .acceptEmail:
            return "registration/email/accept/"
        case .acceptPhone:
            return "registration/phone/accept/"
        case .acceptEmailResendCode:
            return "registration/email/accept/resend/"
        case .acceptPhoneResendCode:
            return "registration/phone/accept/resend/"
        case .loginEmail:
            return "login/email/"
        case .loginPhone:
            return "login/phone/"
        case .loginFB:
            return "login/fb/"
        case .loginVK:
            return "login/vk/"
        case .recoverypassEmail:
            return "recovery_pass/email/"
        case .recoverypassEmailAccept:
            return "recovery_pass/email/accept/"
        case .recoverypassEmailChangePassword:
            return "recovery_pass/email/change/"
        case .recoverypassPhone:
            return "recovery_pass/phone/"
        case .recoverypassPhoneAccept:
            return "recovery_pass/phone/accept/"
        case .recoverypassPhoneChangePassword:
            return "recovery_pass/phone/change/"
        }
    }
    
    var method: Moya.Method {
        return .post
    }
    
    var parameters: [String : Any]? {
        var params: [String : Any] = [:]
        switch self {
        case .registerEmail(let email, let password), .loginEmail(let email, let password), .recoverypassEmailChangePassword(let email, let password):
            params["email"] = email
            params["password"] = password
        case .registerPhone(let phone, let password), .loginPhone(let phone, let password), .recoverypassPhoneChangePassword(let phone, let password):
            params["phone"] = phone
            params["password"] = password
        case .acceptEmail(let email, let code), .recoverypassEmailAccept(let email, let code):
            params["email"] = email
            params["code"] = code
        case .acceptPhone(let phone, let code), .recoverypassPhoneAccept(let phone, let code):
            params["phone"] = phone
            params["code"] = code
        case .acceptEmailResendCode(let email), .recoverypassEmail(let email):
            params["email"] = email
        case .acceptPhoneResendCode(let phone), .recoverypassPhone(let phone):
            params["phone"] = phone
        case .loginFB(let token), .loginVK(let token):
            params["token"] = token
        }
        
        return params
    }
    
    var task: Task {
        return .requestParameters(parameters: parameters ?? [:], encoding: parameterEncoding)
    }
}
