//
//  AuthDataManager.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 10/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import RxSwift
import RxCocoa

enum AuthResult {
    case success
    case ok(token: Token)
    case phoneEmail(phoneEmail: PhoneEmail)
    case error
}

class AuthDataManager: HasDisposeBag {
    
    var api: Network<AuthApi>

    init() {
        self.api = Network<AuthApi>()
    }
    
    // MARK: Register
    
    func registerUser(phoneEmail: PhoneEmail, password: String) -> Observable<AuthResult> {
        var request: AuthApi
        var authData = phoneEmail
        switch phoneEmail.type {
        case .email:
            request = .registerEmail(email: authData.value, password: password)
        case .phone:
            authData.value = convertPhoneIfNeeded(phone: phoneEmail.value)
            request = .registerPhone(phone: authData.value, password: password)
        }
        
        return api.request(request)
            .map { if $0.statusCode < 400 { return .phoneEmail(phoneEmail: authData) } else { return .error } }
    }
    
    func registerCode(phoneEmail: PhoneEmail, code: String) -> Observable<AuthResult> {
        var request: AuthApi
        switch phoneEmail.type {
        case .email:
            request = .acceptEmail(email: phoneEmail.value, code: code)
        case .phone:
            request = .acceptPhone(phone: phoneEmail.value, code: code)
        }
        
        return makeTokenRequest(request: request)
    }
    
    func resendCode(phoneEmail: PhoneEmail) {
        var request: AuthApi
        switch phoneEmail.type {
        case .email:
            request = .acceptEmailResendCode(email: phoneEmail.value)
        case .phone:
            request = .acceptPhoneResendCode(phone: phoneEmail.value)
        }
        
        api.request(request)
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    // MARK: Auth
    
    func authUser(phoneEmail: PhoneEmail, password: String) -> Observable<AuthResult> {
        var request: AuthApi
        switch phoneEmail.type {
        case .email:
            request = .loginEmail(email: phoneEmail.value, password: password)
        case .phone:
            request = .loginPhone(phone: convertPhoneIfNeeded(phone: phoneEmail.value), password: password)
        }
        
        return makeTokenRequest(request: request)
    }
    
    func authSocial(type: OAuth2) -> Observable<AuthResult> {
        
        return loginSocial(type: type)
            .flatMap { [weak self] t -> Observable<AuthResult> in
                
                guard let `self` = self else { return Observable.empty() }
                
                if let token = t {
                    var request: AuthApi
                    
                    if type is Facebook {
                        request = .loginFB(token: token)
                    } else {
                        request = .loginVK(token: token)
                    }
                    
                    return self.api.request(request)
                        .mapObject(to: Token.self)
                        .map { if $0.token != nil { return .ok(token: $0) } else { return .error } }
                } else {
                    return Observable.just(.error)
                }
            }
    }
    
    private func loginSocial(type: OAuth2) -> Observable<String?> {
        
        return Observable.create { obs in
            Simplicity.login(type) { token, error in
                if let e = error {
                    obs.onError(e)
                } else {
                    obs.onNext(token)
                    obs.onCompleted()
                }
            }
            return Disposables.create()
        }
    }
    
    // MARK: Recovery
    
    func recoveryPass(phoneEmail: PhoneEmail) -> Observable<AuthResult> {
        var request: AuthApi
        switch phoneEmail.type {
        case .email:
            request = .recoverypassEmail(email: phoneEmail.value)
        case .phone:
            request = .recoverypassPhone(phone: phoneEmail.value)
        }
        
        return api.request(request)
            .map { if $0.statusCode < 400 { return .phoneEmail(phoneEmail: phoneEmail) } else { return .error } }
    }
    
    func recoveryPassCode(phoneEmail: PhoneEmail, code: String) -> Observable<AuthResult> {
        var request: AuthApi
        switch phoneEmail.type {
        case .email:
            request = .recoverypassEmailAccept(email: phoneEmail.value, code: code)
        case .phone:
            request = .recoverypassPhoneAccept(phone: phoneEmail.value, code: code)
        }
        
        return api.request(request)
            .map { if $0.statusCode < 400 { return .phoneEmail(phoneEmail: phoneEmail) } else { return .error } }
    }
    
    func changePassword(phoneEmail: PhoneEmail, password: String) -> Observable<AuthResult> {
        var request: AuthApi
        switch phoneEmail.type {
        case .email:
            request = .recoverypassEmailChangePassword(email: phoneEmail.value, password: password)
        case .phone:
            request = .recoverypassPhoneChangePassword(phone: phoneEmail.value, password: password)
        }
        
        return api.request(request)
            .map { if $0.statusCode < 400 { return .success } else { return .error } }
    }
    
    // MARK: helpers
    
    private func convertPhoneIfNeeded(phone: String) -> String {
        if phone.first == "8" {
            return phone.replacingCharacters(in: phone.startIndex..<phone.index(phone.startIndex, offsetBy: 1), with: "+7")
        }
        return phone
    }
    
    private func makeTokenRequest(request: AuthApi) -> Observable<AuthResult> {
        return api.request(request)
            .mapObject(to: Token.self)
            .map { if $0.token != nil { return .ok(token: $0) } else { return .error } }
    }
}
