//
//  AuthCoordinator.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 10/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift

final class AuthCoordinator: BaseCoordinator<Void> {
    
    private var window: UIWindow?
    private var nc: UINavigationController!
    private var dependencies: AuthDependency!
    
    init(window: UIWindow?, dependencies: AuthDependency) {
        self.window = window
        self.dependencies = dependencies
        self.nc = UINavigationController()
        nc.navigationBar.setBackgroundImage(UIImage(), for: .default)
        nc.navigationBar.shadowImage = UIImage()
        nc.navigationBar.tintColor = .black
    }
    
    override func start() -> Observable<Void> {
        let viewModel = AuthViewModel(dependencies: dependencies)
        let vc = AuthVC.storyboardInstance()
        vc.viewModel = viewModel
        nc.setViewControllers([vc], animated: true)
        
        viewModel.showRegister.drive(onNext: { [weak self] in self?.showRegistration() })
            .disposed(by: disposeBag)
        
        viewModel.showForgotPass.drive(onNext: { [weak self] in self?.showForgotPassword() })
            .disposed(by: disposeBag)
        
        viewModel.didAuth.asDriver(onErrorJustReturn: .error).drive(onNext: { [weak self] result in
            switch result {
            case .ok(let token):
                
                token.saveToken()
                
                if token.haveInfo {
                    self?.showMainFlow()
                } else {
                    self?.showUserInfo()
                }
                
            default:
                break
            }
        }).disposed(by: disposeBag)
        
        window?.rootViewController = nc
        window?.makeKeyAndVisible()
        return Observable.never()
    }
    
    func showRegistration() {
        let c = RegisterCoordinator(nc: nc, dependencies: dependencies)
        coordinate(to: c)
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    func showForgotPassword() {
        let c = RegisterForgotPassCoordinator(nc: nc, dependencies: dependencies)
        coordinate(to: c)
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    func showUserInfo() {
        
    }
    
    func showMainFlow() {
        
    }
}
