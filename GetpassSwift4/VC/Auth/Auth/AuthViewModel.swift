//
//  AuthViewModel.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 10/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import RxSwift
import RxCocoa

class AuthViewModel: HasDisposeBag {
    
    typealias Dependencies = HasAuthDataManager & HasAuthValidationManager
    
    // MARK: Inputs
    
    let phoneEmail: AnyObserver<String>
    let password: AnyObserver<String>
    let enterTap: AnyObserver<Void>
    let registerTap: AnyObserver<Void>
    let forgotPassTap: AnyObserver<Void>
    let vkTap: AnyObserver<Void>
    let fbTap: AnyObserver<Void>
    
    // MARK: Outputs
    
    let signupEnabled: Driver<Bool>
    let showRegister: Driver<Void>
    let showForgotPass: Driver<Void>
    let didAuth: Driver<AuthResult>
    
    
    private let signingIn = ActivityIndicator()
    
    init(dependencies: Dependencies) {
        
        let dataManager = dependencies.dataManager
        let validationService = dependencies.validationManager
        
        let _phoneEmail = BehaviorSubject<String>(value: "")
        self.phoneEmail = _phoneEmail.asObserver()
        
        let validatedUsername = _phoneEmail
            .map { return validationService.validateUsername($0) }
        
        let userNameType = _phoneEmail
            .map { return validationService.isEmailOrPhone($0) }
        
        let _password = BehaviorSubject<String>(value: "")
        self.password = _password.asObserver()
        
        let validatedPassword = _password
            .map { return validationService.validatePassword($0) }
        
        let _signingIn = signingIn.asObservable()
        
        let usernameAndPassword = Observable.combineLatest(_phoneEmail, userNameType, _password) {
            (phoneEmail: PhoneEmail(value: $0, type: $1), password: $2) }
        
        let _signupEnabled = Observable
            .combineLatest(validatedUsername,
                           validatedPassword,
                           _signingIn) {
                            username, password, signingIn in
                                username.isValid &&
                                password.isValid &&
                                !signingIn
            }.distinctUntilChanged()
        
        signupEnabled = _signupEnabled.asDriver(onErrorJustReturn: false)
        
        let _enterTap = PublishSubject<Void>()
        enterTap = _enterTap.asObserver()
        
        let _registerTap = PublishSubject<Void>()
        registerTap = _registerTap.asObserver()
        showRegister = _registerTap.asDriver(onErrorJustReturn: ())
        
        let _forgotPassTap = PublishSubject<Void>()
        forgotPassTap = _forgotPassTap.asObserver()
        showForgotPass = _forgotPassTap.asDriver(onErrorJustReturn: ())
        
        let auth = _enterTap.withLatestFrom(usernameAndPassword)
            .flatMapLatest { [unowned signingIn] in
                return dataManager
                    .authUser(phoneEmail: $0.phoneEmail, password: $0.password)
                    .trackActivity(signingIn)
        }.asDriver(onErrorJustReturn: .error)
        
        let _fbTap = PublishSubject<Void>()
        fbTap = _fbTap.asObserver()
        
        let fbAuth = _fbTap.flatMap { _ -> Observable<AuthResult> in
            let fb = Facebook()
            fb.scopes = ["public_profile", "email", "user_location", "user_birthday"]
            return dataManager.authSocial(type: fb)
        }.asDriver(onErrorJustReturn: .error)
        
        let _vkTap = PublishSubject<Void>()
        vkTap = _vkTap.asObserver()
        
        let vkAuth = _vkTap.flatMap { _ -> Observable<AuthResult> in
            let vk = VKontakte()
            vk.scopes = ["offline", "email"]
            return dataManager.authSocial(type: vk)
        }.asDriver(onErrorJustReturn: .error)
        
        let _auth = Driver.merge(auth, fbAuth, vkAuth)
        didAuth = _auth.asDriver(onErrorJustReturn: .error)
    }
}
