//
//  AuthVC.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AuthVC: UIViewController {
    
    private lazy var mainView: AuthView = { [unowned self] in return view as! AuthView }()
    var viewModel: AuthViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    func configure() {
        mainView.viewModel = viewModel
    }
    
    deinit {
        print(className)
    }
}
