//
//  AuthView.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 14/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit

class AuthView: UIView {
    
    @IBOutlet weak var txtPhoneEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var btnEnter: UIButton!
    @IBOutlet weak var btnVK: UIButton!
    @IBOutlet weak var btnFB: UIButton!
    @IBOutlet weak var btnForgotPass: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    
    weak var viewModel: AuthViewModel! {
        didSet {
            binds()
        }
    }
    
    func binds() {
        txtPhoneEmail.rx.text.orEmpty.bind(to: viewModel.phoneEmail).disposed(by: rx.disposeBag)
        
        txtPassword.rx.text.orEmpty.bind(to: viewModel.password).disposed(by: rx.disposeBag)
        
        viewModel.signupEnabled
            .drive(onNext: { [weak self] valid in
                self?.btnEnter.isEnabled = valid
                self?.btnEnter.alpha = valid ? 1.0 : 0.5
            }).disposed(by: disposeBag)
        
        btnEnter.rx.tap.bind(to: viewModel.enterTap)
            .disposed(by: rx.disposeBag)
        
        btnForgotPass.rx.tap.bind(to: viewModel.forgotPassTap)
            .disposed(by: rx.disposeBag)
        
        btnRegister.rx.tap.bind(to: viewModel.registerTap)
            .disposed(by: rx.disposeBag)
        
        btnVK.rx.tap.bind(to: viewModel.vkTap)
            .disposed(by: rx.disposeBag)
        
        btnFB.rx.tap.bind(to: viewModel.fbTap)
            .disposed(by: rx.disposeBag)
    }
}
