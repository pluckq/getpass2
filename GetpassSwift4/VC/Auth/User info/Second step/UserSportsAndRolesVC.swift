//
//  UserSportsAndRolesVC.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit

class UserSportsAndRolesVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: UserSportRolesVM!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    deinit {
        print(className)
    }
}
