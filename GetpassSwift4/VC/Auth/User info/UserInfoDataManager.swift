//
//  UserInfoDataManager.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 15/01/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import RxSwift

class UserInfoDataManager: HasDisposeBag {
    
    var api: Network<UserInfoApi>
    
    init() {
        self.api = Network<UserInfoApi>()
    }
    
    func getCountries() -> Observable<Country> {
        return api.request(.getCountries)
            .mapObject(to: Country.self)
    }

    func getCities(countryID: Int) -> Observable<City> {
        return api.request(.getCities(countryID: countryID))
            .mapObject(to: City.self)
    }
    
    func getSports() -> Observable<[Sport]> {
        return api.request(.getSports)
            .mapArray(of: Sport.self)
    }
    
    func getSportLines(sportID: Int) -> Observable<[SportLine]> {
        return api.request(.getSportLinesForSport(sportID: sportID))
            .mapArray(of: SportLine.self)
    }
    
    func getProfile() -> Observable<Profile> {
        return api.request(.getForm)
            .mapObject(to: Profile.self)
    }
    
    func postForm(form: RegistrationForm) -> Observable<Bool> {
        return api.request(.postForm(form: form))
            .map { if $0.statusCode < 400 { return true } else { return false } }
    }
    
    func postSports(sports: [RegistrationRolesAndSports]) -> Observable<Bool> {
        return api.request(.postUserSports(sports))
            .map { if $0.statusCode < 400 { return true } else { return false } }
    }
    
}
