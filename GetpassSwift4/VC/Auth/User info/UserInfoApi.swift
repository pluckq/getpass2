//
//  UserInfoApi.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 15/01/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import Moya

enum UserInfoApi {
    case getCountries
    case getCities(countryID: Int)
    case getSports
    case getSportLinesForSport(sportID: Int)
    case getForm
    case postForm(form: RegistrationForm)
    case postUserSports([RegistrationRolesAndSports])
}

extension UserInfoApi: NetworkingProtocol {
    var path: String {
        switch self {
        case .getCountries:
            return "countries/"
        case .getCities(let country_id):
            return "countries/\(country_id)/cities/"
        case .getSports:
            return "sports/with_sport_lines/"
        case .getSportLinesForSport(let sport_id):
            return "sports/\(sport_id)/sport_lines/"
        case .getForm:
            return "me/registration_form/step_1/"
        case .postForm:
            return "me/registration_form/step_1/"
        case .postUserSports:
            return "me/registration_form/step_2/"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .postUserSports, .postForm:
            return .post
        default:
            return .get
        }
    }
    
    var parameters: [String : Any]? {
        var params: [String : Any] = [:]
        switch self {
        case .postForm(let form):
            params = form.marshaled()
        case .postUserSports(let sports):
            params["sports"] = sports.map{$0.marshaled()}
        default: break
        }
        
        return params
    }
    
    var task: Task {
        return .requestParameters(parameters: parameters ?? [:], encoding: parameterEncoding)
    }
}
