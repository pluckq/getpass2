//
//  UserInfoCoordinator.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 03/01/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import RxSwift

class UserInfoCoordinator: BaseCoordinator<Void> {
    
    private var nc: UINavigationController!
    
    init(nc: UINavigationController) {
        self.nc = nc
    }
    
    override func start() -> Observable<Void> {
        let vc = UserInfoVC()
        nc.setViewControllers([vc], animated: true)
        
        return Observable.never()
    }

}
