//
//  UserInfoVC.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Eureka

class UserInfoVC: FormViewController {
    
    var viewModel: UserInfoVM!
    
    enum sections: String {
        case mainInfo = "Основная информация"
        case additionalInfo = "Дополнительная информация"
        case social = "Профили в социальных сетях"
    }
    
    enum rows: String {
        case name = "Имя"
        case lastName = "Фамилия"
        case gender = "Пол"
        case birth = "Дата рождения"
        case country = "Страна"
        case city = "Город"
        case height = "Рост"
        case weight = "Вес"
        case vk = "VK"
        case fb = "FB"
    }
    
    enum validationErrors: String {
        case name = "Поле имя обязательное"
        case lastName = "Поле фамилия обязательное"
        case gender = "Поле пол обязательное"
        case birth = "Дата рождения"
        case country = "Страна"
        case city = "Город"
    }
    
    private lazy var mainSection = AppSection(sections.mainInfo.rawValue){_ in}
    
    private lazy var additionalInfoSection = AppSection(sections.additionalInfo.rawValue){_ in}
    
    private lazy var socialSection = AppSection(sections.social.rawValue){_ in}
    
    
    private lazy var titleRow: TextAreaRow = TextAreaRow() {
        $0.value = "Благодарим вас за регистрацию! Для начала работы нам важно узнать друг о друге побольше, пожалуйста заполните анкету ниже."
        }.cellUpdate { cell, row in
            cell.textView.isEditable = false
            cell.textView.textColor = .black
            cell.textView.backgroundColor = .white
            cell.backgroundColor = .white
    }
    
    private lazy var nameRow = TextRow(rows.name.rawValue) {
        $0.title = rows.name.rawValue
        $0.add(rule: RuleRequired(msg: validationErrors.name.rawValue))
        }.onRowValidationChanged { cell, row in if !row.isValid {cell.textLabel?.textColor = .red}}
    
    private lazy var lastNameRow = TextRow(rows.lastName.rawValue) {
        $0.title = rows.lastName.rawValue
        $0.add(rule: RuleRequired(msg: validationErrors.lastName.rawValue))
        }.onRowValidationChanged { cell, row in if !row.isValid {cell.textLabel?.textColor = .red}}
    
    private lazy var genderRow = PickerInlineRow<Gender>(rows.gender.rawValue) {
        $0.title = rows.gender.rawValue
        $0.options = [Gender.M, Gender.F]
        $0.displayValueFor = {
            return $0?.getDescription()
        }
        $0.add(rule: RuleRequired(msg: validationErrors.gender.rawValue))
        }.onRowValidationChanged { cell, row in if !row.isValid {cell.textLabel?.textColor = .red}}
    
    private lazy var birthRow = DateInlineRow(rows.birth.rawValue) {
        $0.title = rows.birth.rawValue
        $0.add(rule: RuleRequired(msg: validationErrors.birth.rawValue))
        }.onRowValidationChanged { cell, row in if !row.isValid { cell.textLabel?.textColor = .red}}
    
    private lazy var countryRow = PickerInlineRow<String>(rows.country.rawValue) {
        $0.title = rows.country.rawValue
        //                $0.value = profile.city?.country.name
        //                $0.options = countries.map {$0.name!}
        $0.add(rule: RuleRequired(msg: validationErrors.country.rawValue))
        }.cellUpdate { [weak self] cell, row in
            //                    row.options = self?.countries.map{$0.name!} ?? []
        }.onChange { [weak self] value in
            //                    if let country = self?.countries.filter({$0.name == value.value}).first {
            //                        self?.form.rowBy(tag: "city")?.baseValue = nil
            //                        self?.form.rowBy(tag: "city")?.updateCell()
            //                        self?.getCities(countryId: country.id)
            //                    }
        }.onRowValidationChanged { cell, row in if !row.isValid {cell.textLabel?.textColor = .red}}
    
    private lazy var cityRow = SuggestionAccessoryRow<City>(rows.city.rawValue) { [weak self] in
        $0.title = rows.city.rawValue
        //                $0.value = self?.profile.city
        //                $0.filterFunction = { [weak self] text in
        //                    self?.cities.filter{$0.name.lowercased().contains(text.lowercased())} ?? []
        //                }
        $0.hidden = Condition.function([rows.country.rawValue], {
            if ($0.rowBy(tag: rows.country.rawValue) as? PickerInlineRow<String>)?.value != nil {
                return false
            } else {
                return true
            }
        })
        }.onRowValidationChanged { cell, row in if !row.isValid {cell.textField.textColor = .red}}
    
    private lazy var heightRow = IntRow(rows.height.rawValue) {
        $0.title = rows.height.rawValue
    }
    
    private lazy var weightRow = IntRow(rows.weight.rawValue) {
        $0.title = rows.weight.rawValue
    }
    
    private lazy var vkRow = URLRow(rows.vk.rawValue) {
        $0.title = rows.vk.rawValue
    }
    
    private lazy var fbRow = URLRow(rows.fb.rawValue) {
        $0.title = rows.fb.rawValue
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        configureEureka()
        binds()
    }
    
    func configure() {
        setupVC()
        setupRows()
    }
    
    func configureEureka() {
        form
            +++ titleRow
            +++ mainSection
            <<< nameRow
            <<< lastNameRow
            <<< genderRow
            <<< birthRow
            <<< countryRow
            <<< cityRow
            +++ additionalInfoSection
            <<< heightRow
            <<< weightRow
            +++ socialSection
            <<< vkRow
            <<< fbRow
    }
    
    func binds() {
        
    }
    
    deinit {
        print(className)
    }
}
