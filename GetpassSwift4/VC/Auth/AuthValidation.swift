//
//  AuthValidation.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 15/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import RxSwift

enum ValidationResult {
    case ok(message: String)
    case empty
    case validating
    case failed(message: String)
}

extension ValidationResult {
    var isValid: Bool {
        switch self {
        case .ok:
            return true
        default:
            return false
        }
    }
}

enum AuthEmailPhoneValidation {
    case email
    case phone
}

protocol AuthValidationService {
    func validateUsername(_ username: String) -> ValidationResult
    func isEmailOrPhone(_ username: String) -> AuthEmailPhoneValidation
    func validatePassword(_ password: String) -> ValidationResult
    func validateRepeatedPassword(_ password: String, repeatedPassword: String) -> ValidationResult
    func validateCode(_ code: String) -> ValidationResult
}

class AuthValidationManager: AuthValidationService {
    func validateUsername(_ username: String) -> ValidationResult {
        let isValid: ValidationResult = username.isNotEmpty && username.isEmail || username.isNumeric ? .ok(message: "Valid") : .failed(message: "Неверный логин")
        return isValid
    }
    
    func isEmailOrPhone(_ username: String) -> AuthEmailPhoneValidation {
        let isValid: AuthEmailPhoneValidation = username.isEmail ? .email : .phone
        return isValid
    }
    
    func validatePassword(_ password: String) -> ValidationResult {
        return password.isNotEmpty && password.count > 0 ? ValidationResult.ok(message: "Valid") : .failed(message: "Неверный пароль")
    }
    
    func validateRepeatedPassword(_ password: String, repeatedPassword: String) -> ValidationResult {
        return password == repeatedPassword ? ValidationResult.ok(message: "Valid") : .failed(message: "Неверный пароль")
    }
    
    func validateCode(_ code: String) -> ValidationResult {
        return code.count == 4 ? .ok(message: "Valid") : .failed(message: "")
    }
}
