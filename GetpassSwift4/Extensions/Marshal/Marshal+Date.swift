//
//  Marshal+Date.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 14/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

extension Date : ValueType {
    public static func value(from object: Any) throws -> Date {
        
        guard let dateString = object as? String else {
            throw MarshalError.typeMismatch(expected: String.self, actual: type(of: object))
        }
        
        if let dateFullMS = dateString.date(withFormat: String.DateFormats.full) {
            return dateFullMS
        }
        
        if let dateNoMS = dateString.date(withFormat: String.DateFormats.withoutMS) {
            return dateNoMS
        }

        throw MarshalError.typeMismatch(expected: "date string", actual: dateString)
    }
}
