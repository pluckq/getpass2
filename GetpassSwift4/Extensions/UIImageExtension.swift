//
//  UIImageExtension.swift
//  Eyebrows
//
//  Created by vadim vitvickiy on 08.12.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit

extension UIImage {
    
    func toCIImage() -> CIImage {
        return self.ciImage ?? CIImage(cgImage: self.cgImage!)
    }
    
    func toUIImage(image: CIImage) -> UIImage {
        return UIImage(ciImage: image)
    }
    
    func toData() -> Data {
        return UIImageJPEGRepresentation(self, 7)!
    }
    
    func toRoundedCorners() -> UIImage? {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        let path = UIBezierPath(roundedRect: rect, cornerRadius: size.width)
        let context = UIGraphicsGetCurrentContext()
        context?.saveGState()
        path.addClip()
        draw(in: rect)
        let roundedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return roundedImage
    }
}
