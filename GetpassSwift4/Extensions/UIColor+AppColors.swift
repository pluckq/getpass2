//
//  UIColor+AppColors.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 30/10/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit
import SwiftyUtils

extension UIColor {
    struct AppColor {
        struct Gray {
            static let Dark = UIColor(hex: "607D8B")
            static let Light = UIColor(hex: "ECEFF1")
        }
        
        static let selectedColor = UIColor(hex: "CFD8DC")
        static let Orange = UIColor(hex: "FF5722")
    }
}
