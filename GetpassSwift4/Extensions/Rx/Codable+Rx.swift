////
////  Codable+Rx.swift
////  Getpass
////
////  Created by vadim vitvickiy on 18/09/2017.
////  Copyright © 2017 vadim vitvickiy. All rights reserved.
////
//
//import RxSwift
//import Moya
//
//public extension ObservableType where E == Response {
//    
//    public func mapObject<T: Codable>(_ type: T.Type) -> Observable<T> {
//        return flatMap { response -> Observable<T> in
//            return Observable.just(try response.mapObject(type))
//        }
//    }
//    
//    public func mapArray<T: Codable>(_ type: T.Type) -> Observable<[T]> {
//        return flatMap { response -> Observable<[T]> in
//            return Observable.just(try response.mapArray(type))
//        }
//    }
//}
//
//public extension PrimitiveSequence where TraitType == SingleTrait, ElementType == Response {
//    
//    public func mapObject<T: Codable>(_ type: T.Type) -> Single<T> {
//        return flatMap { response -> Single<T> in
//            return Single.just(try response.mapObject(type))
//        }
//    }
//    
//    public func mapArray<T: Codable>(_ type: T.Type) -> Single<[T]> {
//        return flatMap { response -> Single<[T]> in
//            return Single.just(try response.mapArray(type))
//        }
//    }
//}
//
//public extension Response {
//    
//    public func mapObject<T: Codable>(_ type: T.Type) throws -> T {
//        let jsonDecoder = JSONDecoder()
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = String.DateFormats.full
//        jsonDecoder.dateDecodingStrategy = .formatted(dateFormatter)
//        do {
//            return try jsonDecoder.decode(type, from: data)
//        } catch {
//            print(error)
//            throw MoyaError.jsonMapping(self)
//        }
//    }
//    
//    public func mapArray<T: Codable>(_ type: T.Type) throws -> [T] {
//        let jsonDecoder = JSONDecoder()
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = String.DateFormats.full
//        jsonDecoder.dateDecodingStrategy = .formatted(dateFormatter)
//        do {
//            return try jsonDecoder.decode([T].self, from: data)
//        } catch {
//            print(error)
//            throw MoyaError.jsonMapping(self)
//        }
//    }
//}
//
