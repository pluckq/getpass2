//
//  HUD.swift
//  Getpass
//
//  Created by vadim vitvickiy on 13.02.17.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift
import Moya

extension ActivityIndicator {
    func bind(bag: DisposeBag) {
        let hud = StatusBarIndicator()
        self.asObservable()
            .bind(to: hud.rx_showHUD)
            .disposed(by: bag)
    }
}

class StatusBarIndicator {
    public var rx_showHUD: AnyObserver<Bool> {
        return AnyObserver { event in
            DispatchQueue.main.async {
                switch (event) {
                case .next(let value):
                    if value {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = true
                    } else {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
                case .error:
                    break
                case .completed:
                    break
                }
            }
        }
    }
}

