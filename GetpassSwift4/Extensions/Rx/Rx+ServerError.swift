//
//  Rx+ServerError.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 17/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import RxSwift
import Moya

extension Observable {
    func rxServerError() -> Observable<Element> {
        return self.do(onNext: { response in
            guard let r = response as? Response else { return }
            self.handleError(response: r)
        }, onError: { error in
            guard let e = error as? MoyaError else { throw error }
            guard case .statusCode(let response) = e else { throw error }
            self.handleError(response: response)
        })
    }
    
    private func handleError(response: Response) {
//        if response.statusCode == 400 {
//            if let error = try? response.mapArray(of: FieldError.self).first?.errors.first {
//                if error?.code == "not_accepted" {
//
//                }
//            }
//        }
        if response.statusCode >= 400 {
            
            debugPrint(response.statusCode)
            
            var message: String?
            
            if let errors = try? response.mapArray(of: FieldError.self) {
                message = errors.first?.errors.first?.message
            } else if let json = try? response.mapJSON() as? [String: [Any]] {
                if let errors = json?.first?.value {
                    message = errors[0] as? String
                }
            } else if let error = try? response.mapJSON() as? [String: Any] {
                message = error?.values.first as? String
            }
            
            GetpassSwift4.mainWindow()?.showAlert(title: "Ошибка \(response.statusCode)", message: message ?? "")
        }
    }
}
