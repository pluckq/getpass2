//
//  ScrollViewExtension.swift
//  CityFitnes
//
//  Created by vadim vitvickiy on 14.04.17.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

extension Reactive where Base: UIScrollView {
    var reachedBottom: ControlEvent<Void> {
        let observable = contentOffset
            .flatMap { [weak base] contentOffset -> Observable<Void> in
                guard let scrollView = base else {
                    return Observable.empty()
                }
                
                let visibleHeight = scrollView.frame.height - scrollView.contentInset.top - scrollView.contentInset.bottom
                let y = contentOffset.y + scrollView.contentInset.top
                let threshold = max(0.0, scrollView.contentSize.height - visibleHeight)
                
                return y > threshold ? Observable.just(Void()) : Observable.empty()
        }
        
        return ControlEvent(events: observable)
    }
}

extension Reactive where Base: UIScrollView {
    var scrolledToBottom: ControlEvent<Void> {
        let event = Observable
            .zip(contentOffset.skip(1), contentOffset)
            .flatMap { [weak base] current, previous -> Observable<Void> in
                guard let scrollView = base, current.y - previous.y > 0 && current.y > 0 else {
                    return .empty()
                }
                
                let y = current.y + scrollView.contentInset.top
                let visibleHeight = scrollView.frame.height - scrollView.contentInset.top - scrollView.contentInset.bottom
                let threshold = max(0.0, scrollView.contentSize.height - visibleHeight)
                
                return y > threshold ? .just(Void()) : .empty()
            }
            .throttle(1.0, latest: false, scheduler: MainScheduler.instance)
        
        return ControlEvent(events: event)
    }
}
