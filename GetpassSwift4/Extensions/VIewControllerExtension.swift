//
//  VIewControllerExtension.swift
//  Getpass
//
//  Created by vadim vitvickiy on 19.12.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.view.tintColor = UIColor.AppColor.Gray.Dark
        alertController.addAction(UIAlertAction(title: "ОК", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    func showAlert(title: String, message: String, actions: [UIAlertAction]) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.view.tintColor = UIColor.AppColor.Gray.Dark
        actions.forEach { alertController.addAction($0) }
        alertController.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    private class func storyboardInstancePrivate<T: UIViewController>() -> T {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateInitialViewController() as! T
    }
    
    class func storyboardInstance() -> Self {
        return storyboardInstancePrivate()
    }
    
    func addHideKeyboardGesture() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
