//
//  DataExtension.swift
//  Eyebrows
//
//  Created by vadim vitvickiy on 08.12.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit

extension Data {
    func toImage() -> UIImage {
        return UIImage(data: self)!
    }
}
