//
//  TrainingRepeatType.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit

struct TrainingRepeat: Equatable {
    
    static func ==(lhs: TrainingRepeat, rhs: TrainingRepeat) -> Bool {
        return lhs.type == rhs.type
    }
    
    var type: RepeatType?
    var customRepeat: CustomRepeatType?
    var customDays: [CustomDaysOfWeek]?
    
}
