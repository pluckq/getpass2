//
//  TrainingLocation.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

struct TrainingLocation: Identifiable, Unmarshaling, Marshaling {

    var id: Int = 0
    var name: String?
    var description: String?
    var lat: Double = 0.0
    var lng: Double = 0.0
    var city: City?
    
    init(name: String?, description: String?, lat: Double, lng: Double) {
        self.name = name
        self.description = description
        self.lat = lat
        self.lng = lng
    }
    
    init(object: MarshaledObject) throws {
        id = try object.value(for: "id")
        name = try object.value(for: "name")
        description = try object.value(for: "description")
        lat = try object.value(for: "lat")
        lng = try object.value(for: "lng")
        city = try object.value(for: "city")
    }
    
    func marshaled() -> [String: Any] {
        var params: [String: Any] = [:]
        params["name"] = name
        params["description"] = description
        params["lat"] = lat
        params["lng"] = lng
        return params
    }
}
