//
//  Training.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

struct Training: Unmarshaling {

    var id: Int = 0
    var name: String?
    var datetime: Date?
    var description: String?
    var location: TrainingLocation?
    var price: Int?
    var teamName: String?
    var aggregatedInviteID: Int?
    var inviteID: Int?
    var secondNotificationID: Int?
    var members: [TeamMember]?
    var status = TrainingStatus.not_member
    var membersCount: Int = 0
    var isOpen: Bool = false
    var isRepetitive: Bool = false
    var canEdit: Bool = false
    var isRequestSent: Bool = false
    
    init(object: MarshaledObject) throws {
        id = try object.value(for: "id")
        isOpen = try object.value(for: "is_open")
        isRepetitive = try object.value(for: "is_repetitive")
        teamName = try object.value(for: "team_name")
        canEdit = try object.value(for: "can_edit")
        members = try object.value(for: "training_members")
        aggregatedInviteID = try object.value(for: "aggregated_invite_id")
        inviteID = try object.value(for: "invite_id")
        secondNotificationID = try object.value(for: "second_notification_id")
        status = try object.value(for: "status")
        isRequestSent = try object.value(for: "is_request_sent")
        membersCount = try object.value(for: "max_members")
        name = try object.value(for: "name")
        datetime = try object.value(for: "datetime")
        description = try object.value(for: "description")
        price = try object.value(for: "price")
        location = try object.value(for: "location")
    }   
}

struct TrainingCreate: Marshaling {
    
    var trainingTypeID: Int?
    var name: String?
    var datetime: Date?
    var lastDateTime: Date?
    var description: String?
    var location: TrainingLocation?
    var notificationDelay: Int?
    var repeatType: String?
    var customRepeatDays: [String]?
    var customRepeatType: String?
    var membersID: [Int]?
    var price: Int?
    
    func marshaled() -> [String: Any] {
        var params: [String: Any] = [:]
        params["training_type_id"] = trainingTypeID
        params["name"] = name
        params["datetime"] = datetime
        params["last_datetime"] = lastDateTime
        params["notification_delay"] = notificationDelay
        params["repetitive_type"] = repeatType
        params["custom_days_of_week"] = customRepeatDays
        params["custom_repetitive_type"] = customRepeatType
        params["members_ids"] = membersID
        params["price"] = price
        params["location"] = location
        params["description"] = description
        return params
    }
    
}
