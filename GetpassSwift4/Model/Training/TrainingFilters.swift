//
//  TrainingFilters.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

struct TrainingFilters: Marshaling {

    var cityID: Int?
    var startDate: Date?
    var endDate: Date?
    var startTime: Date?
    var endTime: Date?
    var startPrice: Int?
    var endPrice: Int?
    
    func marshaled() -> [String: Any] {
        var params: [String: Any] = [:]
        params["city_id"] = cityID
        params["date_start"] = startDate
        params["date_end"] = endDate
        params["time_start"] = startTime
        params["time_end"] = endTime
        params["price_start"] = startPrice
        params["price_end"] = endPrice
        return params
    }
}
