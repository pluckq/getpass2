//
//  FieldError.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

struct FieldError: Unmarshaling {

    let field: String
    let errors: [ServerError]
    
    init(object: MarshaledObject) throws {
        field = try object.value(for: "field")
        errors = try object.value(for: "errors")
    }
    
}

struct ServerError: Unmarshaling {
    
    let code: String
    let message: String

    init(object: MarshaledObject) throws {
        code = try object.value(for: "code")
        message = try object.value(for: "message")
    }
    
}
