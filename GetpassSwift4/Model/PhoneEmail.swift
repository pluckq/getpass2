//
//  PhoneEmail.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 06/01/2018.
//  Copyright © 2018 vadim vitvickiy. All rights reserved.
//

import UIKit

struct PhoneEmail {
    
    var value: String
    var type: AuthEmailPhoneValidation

}
