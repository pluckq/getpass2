//
//  RegistrationRolesAndSports.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

struct RegistrationRolesAndSports: Marshaling {

    var name: String?
    var role: Roles?
    var sportID: Int?
    var player: RegistrationPlayer?
    var manager: RegistrationManager?
    
    func marshaled() -> [String: Any] {
        var params: [String: Any] = [:]
        params["sport_id"] = sportID
        params["player"] = player
        params["manager"] = manager
        return params
    }
}

struct RegistrationPlayer: Marshaling {
    
    var about: String?
    var sportLinesID: [Int]?
    var sportLinesNames: [String]?
    
    func marshaled() -> [String: Any] {
        var params: [String: Any] = [:]
        params["about"] = about
        params["sport_lines_id"] = sportLinesID
        return params
    }
    
}

struct RegistrationManager: Marshaling {
    
    var about: String?
    
    func marshaled() -> [String: Any] {
        var params: [String: Any] = [:]
        params["about"] = about
        return params
    }
}
