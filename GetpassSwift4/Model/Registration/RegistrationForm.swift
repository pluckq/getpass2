//
//  RegistrationForm.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

struct RegistrationForm: Marshaling, Unmarshaling {
    
    var firstName: String?
    var lastName: String?
    var gender: String?
    var birthday: String?
    var cityID: Int?
    var city: City?
    var height: Int?
    var weight: Int?
    var vk: String?
    var fb: String?
    
    init(object: MarshaledObject) throws {
        firstName = try object.value(for: "first_name")
        lastName = try object.value(for: "last_name")
        gender = try object.value(for: "gender")
        birthday = try object.value(for: "birthday")
        cityID = try object.value(for: "city_id")
        city = try? object.value(for: "city")
        height = try object.value(for: "height")
        weight = try object.value(for: "weight")
        vk = try object.value(for: "vk")
        fb = try object.value(for: "fb")
    }
    
    func marshaled() -> [String: Any] {
        var params: [String: Any] = [:]
        params["first_name"] = firstName
        params["last_name"] = lastName
        params["gender"] = gender
        params["birthday"] = birthday
        params["city_id"] = cityID
        params["city"] = city
        params["height"] = height
        params["weight"] = weight
        params["vk"] = vk
        params["fb"] = fb
        return params
    }
}
