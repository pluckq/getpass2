//
//  Team.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

struct Team: Unmarshaling {

    var id: Int = 0
    var name: String = ""
    var membersCount: Int = 0
    var city: City!
    var avatar: URL?
    var about: String?
    var sport: Sport?
    var statuses: [Status]?
    var trainings: [UserEvent]?
    var vacancies: [TeamVacancy]?
    var friendlyGames: [UserEvent]?
    var members: [TeamMember]?
    var isManager = false
    var isCaptain = false
    var isPlayer = false
    var playerSportLines: [SportLine]?
    
    init(object: MarshaledObject) throws {
        id = try object.value(for: "id")
        name = try object.value(for: "name")
        membersCount = try object.value(for: "members_count")
        city = try object.value(for: "city")
        avatar = try object.value(for: "avatar")
        statuses = try object.value(for: "statuses")
        trainings = try object.value(for: "trainings")
        vacancies = try object.value(for: "vacancies")
        friendlyGames = try object.value(for: "friendly_games")
        members = try object.value(for: "random_5_members")
        isManager = try object.value(for: "iis_managerd")
        isPlayer = try object.value(for: "is_player")
        sport = try object.value(for: "sport")
        about = try object.value(for: "about")
        playerSportLines = try object.value(for: "my_sport_lines_in_team")
        isCaptain = try object.value(for: "is_captain")
    }
}

struct TeamCreate: Marshaling {
    
    var name: String = ""
    var isCaptain = false
    var isPlayer = false
    var about: String?
    var sportLines: [Int]?
    var cityID: Int = 0
    var sportID: Int = 0
    var capitanID: Int?
    
    func marshaled() -> [String: Any] {
        var params: [String: Any] = [:]
        params["is_captain"] = isCaptain
        params["is_player"] = isPlayer
        params["my_sport_lines_in_team"] = sportLines
        params["about"] = about
        params["city_id"] = cityID
        params["sport_id"] = sportID
        params["captain_user_id"] = capitanID
        return params
    }
}
