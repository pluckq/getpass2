//
//  TeamMember.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

struct TeamMember: Equatable, Unmarshaling {
    
    static func ==(lhs: TeamMember, rhs: TeamMember) -> Bool {
        return lhs.user?.id == rhs.user?.id
    }

    var trainingStatus: MemberTrainingStatus?
    var isCaptain = false
    var isManager = false
    var sportLines: [SportLine]?
    var user: User?
    
    init(object: MarshaledObject) throws {
        isCaptain = try object.value(for: "is_captain")
        isManager = try object.value(for: "is_manager")
        sportLines = try object.value(for: "sport_lines")
        trainingStatus = try object.value(for: "status")
        user = try object.value(for: "user")
    }
}
