//
//  TeamNotificationsSettings.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

struct TeamNotificationsSettings: Marshaling {
    
    var newPlayers = false
    var newTournaments = false
    var newFriendly_games = false
    var changeInfo = false
    var newRequests = false
    
    func marshaled() -> [String: Any] {
        var params: [String: Any] = [:]
        params["new_players"] = newPlayers
        params["new_tournaments"] = newTournaments
        params["new_friendly_games"] = newFriendly_games
        params["change_info"] = changeInfo
        params["new_requests"] = newRequests
        return params
    }
}
