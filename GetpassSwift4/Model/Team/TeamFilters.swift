//
//  TeamFilters.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

struct TeamFilters: Marshaling {

    var city: City?
    var gender: Gender?
    var hasVacancy: Bool?
    var ampluaVacancy: Int?
    var statuses: [Int]?
    var ageStart: Int?
    var ageEnd: Int?
    
    func marshaled() -> TeamNotificationsSettings.MarshalType {
        var params: [String: Any] = [:]
        params["city_id"] = city?.id
        params["has_vacancy"] = hasVacancy
        params["vacancy_gender"] = gender
        params["vacancy_sport_line_id"] = ampluaVacancy
        params["statuses_id"] = statuses
        params["avg_age_start"] = ageStart == 0 ? nil : ageStart
        params["avg_age_end"] = ageEnd == 100 ? nil : ageEnd
        return params
    }
}
