//
//  TeamVacancy.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

struct TeamVacancy: Unmarshaling {

    var id: Int = 0
    var name: String = ""
    var sportLine: SportLine?
    var sportLineID: Int = 0
    var ageStart: Int = 0
    var ageEnd: Int = 0
    var gender: Gender?
    var team: Team?
    var candidates: [User]?
    var isCandidate = false
    
    init(object: MarshaledObject) throws {
        id = try object.value(for: "id")
        name = try object.value(for: "name")
        sportLine = try object.value(for: "sport_line")
        ageStart = try object.value(for: "age_start")
        ageEnd = try object.value(for: "age_end")
        gender = try object.value(for: "gender")
        sportLineID = try object.value(for: "sport_line_id")
        team = try object.value(for: "team")
        candidates = try object.value(for: "candidates")
        isCandidate = try object.value(for: "is_candidate")
    }
}

struct TeamVacancyCreate: Marshaling {
    
    var name: String = ""
    var sportLineID: Int = 0
    var ageStart: Int = 0
    var ageEnd: Int = 0
    var gender: Gender?
    var team: Team?
    var candidates: [User]?
    var isCandidate = false
    
    func marshaled() -> [String: Any] {
        var params: [String: Any] = [:]
        params["name"] = name
        params["age_start"] = ageStart
        params["age_end"] = ageEnd
        params["gender"] = gender
        params["sport_line_id"] = sportLineID
        params["team"] = team
        params["candidates"] = candidates
        params["is_candidate"] = isCandidate
        return params
    }
}
