//
//  Enums.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit

enum Roles: String, RawRepresentable {
    case player
    case manager
    case organizer
    
    init?(name: String) {
        var value: String
        switch name {
        case "Игрок":
            value = "player"
        case "Менеджер":
            value = "manager"
        case "Организатор":
            value = "organizer"
        default:
            value = ""
        }
        self.init(rawValue: value)
    }
    
    func getDescription() -> String {
        switch self {
        case .player:
            return "Игрок"
        case .manager:
            return "Менеджер"
        case .organizer:
            return "Организатор"
        }
    }
}

enum Gender: String, RawRepresentable {
    case M
    case F
    
    init?(name: String) {
        var value: String
        switch name {
        case "Мужской":
            value = "M"
        case "Женский":
            value = "F"
        default:
            value = ""
        }
        self.init(rawValue: value)
    }
    
    func getDescription() -> String {
        switch self {
        case .M:
            return "Мужской"
        case .F:
            return "Женский"
        }
    }
}

enum DidJoin: String {
    case join = "Вы участвуете"
    case notJoin = "Вы не приняли решение"
}

enum CalendarType: String, RawRepresentable {
    case none
    case tr //тренировка
    case fg //товарищеская игра
}

enum State: String, RawRepresentable {
    case none
    case accept = "a"
    case reject = "r"
}

enum Notifications: String, RawRepresentable {
    case none
    //manager
    case friendshipRequest = "friendship_request"
    case acceptOrRejectFriendship = "friendship_accepted_rejected"
    case friendshipDelete = "friendship_delete"
    case acceptOrRejectTeamInvite = "team_invite_accepted_rejected"
    //player
    case teamInvite = "team_invite"
    //vacancy
    case newVacancyCandidat = "new_vacancy_candidate"
    case newPlayer = "new_player"
    //trainings
    case training = "training_invite"
    case repeatTraining = "aggregated_training_invite"
    case trainingConfirmation = "second_training_notification"
    case trainingPlayerRequest = "training_member_request"
    case trainingPlayerAcceptedRejected = "training_member_request_accept_reject"
    
    func getText() -> String {
        switch self {
        case .training:
            return "Вас приглашают на тренировку"
        case .repeatTraining:
            return "Вас приглашают на повторяющиеся тренировки"
        case .trainingConfirmation:
            return "Подтвердите свое участие в тренировке"
        default:
            return ""
        }
    }
}

enum FriendStatuses: String, RawRepresentable {
    case none
    case me
    case friend
    case not_friend
    case subscriber
    case subscribed
}

enum TrainingStatus: String, RawRepresentable {
//         'y'-yes, 'u'-undefined, 'n'-no, 'not_member',
    case y
    case u
    case n
    case not_member
    
    func getDescription() -> String {
        switch self {
        case .y:
            return "Вы участвуете"
        case .u:
            return "Вы не решили"
        case .n:
            return "Вы отказались от участия"
        case .not_member:
            return "Вы можете присоединиться к тренировке"
        }
    }
}

enum MemberTrainingStatus: String, RawRepresentable {
    
    case y
    case u
    
    func getDescription() -> String {
        switch self {
        case .y:
            return "Подтвердил участие"
        case .u:
            return "Не принял решение"
        }
    }
}

//MARK: Training repeats

enum RepeatType: String, RawRepresentable {
    case day
    case week
    case month
    case year
    case custom
    
    init?(name: String) {
        var value: String
        switch name {
        case "Каждый день":
            value = "day"
        case "Каждую неделю":
            value = "week"
        case "Каждый месяц":
            value = "month"
        case "Каждый год":
            value = "year"
        case "Настраиваемый":
            value = "custom"
        default:
            value = ""
        }
        self.init(rawValue: value)
    }
    
    func getDescription() -> String {
        switch self {
        case .day:
            return "Каждый день"
        case .week:
            return "Каждую неделю"
        case .month:
            return "Каждый месяц"
        case .year:
            return "Каждый год"
        case .custom:
            return "Настраиваемый"
        }
    }
    
    static let allStringValues = [day.getDescription(), week.getDescription(), month.getDescription(), year.getDescription(), custom.getDescription()]
}

enum CustomDaysOfWeek: String, RawRepresentable {
    case mo
    case tu
    case we
    case th
    case fr
    case sa
    case su
    
    init?(name: String) {
        var value: String
        switch name {
        case "Понедельник":
            value = "mo"
        case "Вторник":
            value = "tu"
        case "Среда":
            value = "we"
        case "Четверг":
            value = "th"
        case "Пятница":
            value = "fr"
        case "Суббота":
            value = "sa"
        case "Воскресенье":
            value = "su"
        default:
            value = ""
        }
        self.init(rawValue: value)
    }
    
    func getDescription() -> String {
        switch self {
        case .mo:
            return "Понедельник"
        case .tu:
            return "Вторник"
        case .we:
            return "Среда"
        case .th:
            return "Четверг"
        case .fr:
            return "Пятница"
        case .sa:
            return "Суббота"
        case .su:
            return "Воскресенье"
        }
    }
    
    static let allStringValues = [mo.getDescription(), tu.getDescription(), we.getDescription(), th.getDescription(), fr.getDescription(), sa.getDescription(), su.getDescription()]
}

enum CustomRepeatType: String, RawRepresentable {
    case week
    case twoWeek
    case threeWeek
    case fourWeek
    
    init?(name: String) {
        var value: String
        switch name {
        case "Каждую первую неделю":
            value = "week"
        case "Каждую вторую неделю":
            value = "twoWeek"
        case "Каждую третью неделю":
            value = "threeWeek"
        case "Каждую четвертую неделю":
            value = "fourWeek"
        default:
            value = ""
        }
        self.init(rawValue: value)
    }
    
    func getDescription() -> String {
        switch self {
        case .week:
            return "Каждую первую неделю"
        case .twoWeek:
            return "Каждую вторую неделю"
        case .threeWeek:
            return "Каждую третью неделю"
        case .fourWeek:
            return "Каждую четвертую неделю"
        }
    }
    
    func getServerString() -> String {
        switch self {
        case .week:
            return "week"
        case .twoWeek:
            return "2_week"
        case .threeWeek:
            return "3_week"
        case .fourWeek:
            return "4_week"
        }
    }
    
    static let allStringValues = [week.getDescription(), twoWeek.getDescription(), threeWeek.getDescription(), fourWeek.getDescription()]
}
