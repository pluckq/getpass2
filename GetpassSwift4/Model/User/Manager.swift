//
//  Manager.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

struct Manager: Unmarshaling {

    var about: String?
    var teams: [Team]?
    var notifications: [UserEvent]?
    var friends: [User]?
    var friendsCount: Int = 0
    
    init(object: MarshaledObject) throws {
        about = try object.value(for: "about")
        teams = try object.value(for: "random_3_teams")
        notifications = try object.value(for: "calendar")
        friends = try object.value(for: "random_10_friends")
        friendsCount = try object.value(for: "friends_count")
    }
}
