//
//  Profile.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

struct Profile: Unmarshaling {
    
    var sportID: Int = 0
    var player: Player?
    var manager: Manager?
    var sports: [Sport]?
    var user: User?

    init(object: MarshaledObject) throws {
        sportID = try object.value(for: "sport_id")
        player = try object.value(for: "player")
        manager = try object.value(for: "manager")
        sports = try object.value(for: "sports")
        user = try object.value(for: "user")
    }
}
