//
//  User.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

struct User: Identifiable, Unmarshaling {

    var id: Int = 0
    var name: String?
    var firstName: String?
    var lastName: String?
    var city: City?
    var email: String?
    var ageLetter: String = ""
    var age: Int = 0
    var gender: Gender?
    var birthday: String?
    var growth: Int = 0
    var weight: Int = 0
    var avatar: URL?
    var phone: String?
    var vk: String?
    var facebook: String?
    var friendStatus = FriendStatuses.none
    var frNotificationID: Int = 0
    
    init(object: MarshaledObject) throws {
        id = try object.value(for: "id")
        city = try object.value(for: "city")
        email = try object.value(for: "email")
        ageLetter = try object.value(for: "age_letter")
        age = try object.value(for: "age")
        gender = try object.value(for: "gender")
        birthday = try object.value(for: "birthday")
        growth = try object.value(for: "growth")
        lastName = try object.value(for: "last_name")
        firstName = try object.value(for: "first_name")
        weight = try object.value(for: "weight")
        avatar = try object.value(for: "avatar")
        phone = try object.value(for: "phone")
        vk = try object.value(for: "vk")
        facebook = try object.value(for: "facebook")
        friendStatus = try object.value(for: "friend_status")
        frNotificationID = try object.value(for: "fr_notification_id")
    }
}
