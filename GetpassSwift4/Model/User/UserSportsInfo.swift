//
//  UserSportsInfo.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

struct UserSportsInfo: Unmarshaling {
    
    var hasOrganizer: Bool = false
    var hasManager: Bool = false
    var hasPlayer: Bool = false
    var playerSportLines: [SportLine]?
    var sport: Sport?
    
    init(object: MarshaledObject) throws {
        hasOrganizer = try object.value(for: "has_organizer")
        hasManager = try object.value(for: "has_manager")
        hasPlayer = try object.value(for: "has_player")
        playerSportLines = try object.value(for: "player_sport_lines")
        sport = try object.value(for: "sport")
    }
}
