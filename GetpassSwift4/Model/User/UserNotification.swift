//
//  UserNotification.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

struct UserNotification: Unmarshaling {

    var id: Int = 0
    var name: String = ""
    var date: Date?
    var type = Notifications.none
    var status = State.none
    var user: User?
    var team: Team?
    var vacancy: TeamVacancy?
    var sport: Sport?
    var training: Training?

    init(object: MarshaledObject) throws {
        id = try object.value(for: "id")
        name = try object.value(for: "name")
        date = try object.value(for: "date")
        type = try object.value(for: "type")
        status = try object.value(for: "status")
        user = try object.value(for: "user")
        team = try object.value(for: "team")
        vacancy = try object.value(for: "vacancy")
        sport = try object.value(for: "sport")
        training = try object.value(for: "training")
    }
}
