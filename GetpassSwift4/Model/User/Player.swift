//
//  Player.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

struct Player: Unmarshaling {

    var statuses: [Status]?
    var about: String?
    var sportLines: [SportLine]?
    var teams: [Team]?
    var notifications: [UserEvent]?
    var friends: [User]?
    var friendsCount: Int = 0
    
    init(object: MarshaledObject) throws {
        statuses = try object.value(for: "statuses")
        about = try object.value(for: "about")
        sportLines = try object.value(for: "sport_lines")
        teams = try object.value(for: "random_3_teams")
        notifications = try object.value(for: "calendar")
        friends = try object.value(for: "random_10_friends")
        friendsCount = try object.value(for: "friends_count")
    }
}
