//
//  Sport.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

struct Sport: Unmarshaling {

    var id: Int = 0
    var name: String = ""
    var sportLines: [SportLine]?
    var image: URL?
    var firstColor: String = ""
    var secondColor: String = ""
    
    init(object: MarshaledObject) throws {
        id = try object.value(for: "id")
        name = try object.value(for: "name")
        sportLines = try? object.value(for: "sport_lines")
        image = try object.value(for: "image")
        firstColor = try object.value(for: "first_color")
        secondColor = try object.value(for: "second_color")
    }
}
