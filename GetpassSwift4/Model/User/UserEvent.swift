//
//  UserEvent.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

struct UserEvent: Unmarshaling {

    var id: Int = 0
    var uniqueID: Int = 0
    var name: String = ""
    var type: CalendarType = .none
    var date: Date?
    var firstTeamName: String = ""
    var secondTeamName: String = ""
    var locationName: String = ""
    var teamName: String = ""
    var membersMaxCount: Int = 0
    var membersCount: Int = 0
    var confirmed: Bool = false
    var status = ""
    
    init(object: MarshaledObject) throws {
        firstTeamName = try object.value(for: "first_team_name")
        secondTeamName = try object.value(for: "second_team_name")
        locationName = try object.value(for: "locationName")
        id = try object.value(for: "id")
        uniqueID = try object.value(for: "uniqueId")
        teamName = try object.value(for: "teamName")
        date = try object.value(for: "date")
        type = try object.value(for: "type")
        membersMaxCount = try object.value(for: "membersMaxCount")
        membersCount = try object.value(for: "membersCount")
        name = try object.value(for: "name")
        confirmed = try object.value(for: "confirmed")
        status = try object.value(for: "status")
    }
}

