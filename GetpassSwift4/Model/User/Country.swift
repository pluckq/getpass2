//
//  Country.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

struct Country: Marshaling, Unmarshaling, Identifiable, SuggestionValue {

    var id: Int = 0
    var name: String?
    
    init?(string stringValue: String) {
        return nil
    }
    
    var suggestionString: String {
        return name ?? ""
    }
    
    init(object: MarshaledObject) throws {
        id = try object.value(for: "id")
        name = try object.value(for: "name")
    }
    
    func marshaled() -> [String: Any] {
        var params: [String: Any] = [:]
        params["id"] = id
        params["name"] = name
        return params
    }
}
