//
//  Status.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

struct Status: Unmarshaling {

    var id: Int = 0
    var name: String = ""
    var dissonantStatuses: [DissonantStatus]?
    var isFilter = false
    
    init(object: MarshaledObject) throws {
        id = try object.value(for: "id")
        name = try object.value(for: "name")
        dissonantStatuses = try object.value(for: "dissonant_statuses")
        isFilter = try object.value(for: "is_filter")
    }
}

struct DissonantStatus: Unmarshaling {
    
    var id: Int = 0
    var name: String = ""
    
    init(object: MarshaledObject) throws {
        id = try object.value(for: "id")
        name = try object.value(for: "name")
    }
}
