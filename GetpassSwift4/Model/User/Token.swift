//
//  Token.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Marshal

struct Token: Unmarshaling {

    var token: String?
    var haveInfo = false
    
    init() {
        
    }
    
    init(object: MarshaledObject) throws {
        token = try object.value(for: "token")
        haveInfo = try object.value(for: "have_info")
    }
    
    func saveToken() {
        if let t = token {
            keychain.set(t, forKey: "token")
        }
    }
}
