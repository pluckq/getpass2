//
//  AutoSizeLabelView.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit

class AutoSizeLabelView: UIView {
    
    lazy var label: UILabel = {
        var result = UILabel()
        result.translatesAutoresizingMaskIntoConstraints = false
        result.numberOfLines = 0
        return result
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(label)
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[label]-15-|", options: NSLayoutFormatOptions.alignAllLastBaseline, metrics: nil, views: ["label": label]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-15-[label]-20-|", options: NSLayoutFormatOptions.alignAllLeft, metrics: nil, views: ["label": label]))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        label.preferredMaxLayoutWidth = frame.size.width
        superview?.setNeedsLayout()
    }
}

class StaticLabelView: UIView {
    
    lazy var label: UILabel = {
        var result = UILabel()
        result.translatesAutoresizingMaskIntoConstraints = false
        result.numberOfLines = 0
        return result
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(label)
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[label]-15-|", options: NSLayoutFormatOptions.alignAllLastBaseline, metrics: nil, views: ["label": label]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[label]-|", options: NSLayoutFormatOptions.alignAllLeft, metrics: nil, views: ["label": label]))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        label.preferredMaxLayoutWidth = frame.size.width
        superview?.setNeedsLayout()
    }
}
