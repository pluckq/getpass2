//
//  TrainingRepeatsRow.swift
//  Getpass
//
//  Created by vadim vitvickiy on 06/10/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Eureka

final class TrainingRepeatCell: PushSelectorCell<TrainingRepeat> {
    
    public override func setup() {
        super.setup()
        backgroundColor = UIColor.AppColor.Gray.Light
        height = {eurekaCellHeight}
        layoutMargins = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        layoutMargins.left = eurekaMargin
        layoutMargins.right = eurekaMargin
        preservesSuperviewLayoutMargins = true
        contentView.preservesSuperviewLayoutMargins = true
        detailTextLabel?.textColor = UIColor.AppColor.Gray.Dark
        textLabel?.textColor = .black
    }
    
    public override func update() {
        super.update()
        if let type = row.value {
            switch type.type {
            case .custom?:
                if type.customDays.isNilOrEmpty {
                    row.value = nil
                    row.title = "Укажите период повторения"
                    break
                }
                row.title = "\(type.customRepeat?.getDescription() ?? "") \(type.customDays?.map{$0.getDescription()}.joined(separator: ", ") ?? "")"
            default:
                row.title = type.type?.getDescription()
            }
        }
    }
}

final class TrainingRepeatRow<Cell: TrainingRepeatCell>: OptionsRow<Cell>, PresenterRowType {
    
    typealias PresentedControllerType = TrainingRepeatTypeVC
    var presentationMode: PresentationMode<TrainingRepeatTypeVC>?
    var onPresentCallback: ((FormViewController, TrainingRepeatTypeVC) -> Void)?
    
    public required init(tag: String?) {
        super.init(tag: tag)
        
        presentationMode = .show(
            controllerProvider: ControllerProvider<TrainingRepeatTypeVC>.callback {
                return TrainingRepeatTypeVC()
            },
            onDismiss: { vc in vc.navigationController?.popViewController(animated: true) })
        
        displayValueFor = { trainingRepeat in
            return trainingRepeat?.customRepeat?.getDescription()
        }
    }
}

class TrainingRepeatTypeVC: FormViewController, TypedRowControllerType {
    
    enum rows: String, RawRepresentable {
        case type
    }
    
    public var row: RowOf<TrainingRepeat>!
    public var onDismissCallback : ((UIViewController) -> ())?
    
    let selectTypeRow = AppSelectionSection("Выберите тип повторения:", selectionType: .singleSelection(enableDeselection: false)) {_ in}
    let selectCustomDayRow = AppSelectionSection("Выберите дни повторения:", selectionType: .multipleSelection) {_ in}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        setupForm()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        row.value = createRepeatType()
        
        DispatchQueue.main.async { [weak self] in
            self?.row.reload()
        }
    }
    
    func configure() {
        title = "Период повторения"
        setupVC()
    }
    
    func createRepeatType() -> TrainingRepeat {
        let values = form.values()
        let type = selectTypeRow.selectedRow()?.value != nil ? RepeatType(name: selectTypeRow.selectedRow()?.value ?? "") : nil
        let customRepeatType = CustomRepeatType(name: values[rows.type.rawValue] as? String ?? "")
        let customRepeatDays = selectCustomDayRow.selectedRows().count > 0 ? selectCustomDayRow.selectedRows().map{CustomDaysOfWeek(name: $0.selectableValue!)!} : nil
        let trainingRepeat = TrainingRepeat(type: type, customRepeat: customRepeatType, customDays: customRepeatDays)
        return trainingRepeat
    }
    
    func setupForm() {
        
        let customTypeRow = PickerInlineRow<String>(rows.type.rawValue) {
            $0.title = "Повторять"
            $0.options = CustomRepeatType.allStringValues
            $0.value = row.value?.customRepeat?.getDescription() ?? $0.options.first
        }
        
        customTypeRow.hidden = Condition(booleanLiteral: true)
        selectCustomDayRow.hidden = Condition(booleanLiteral: true)
        
        selectTypeRow.onSelectSelectableRow = { [weak self] cell, row in
            if RepeatType(name: row.value ?? "") == .custom {
                customTypeRow.hidden = Condition(booleanLiteral: false)
                self?.selectCustomDayRow.hidden = Condition(booleanLiteral: false)
            } else {
                customTypeRow.hidden = Condition(booleanLiteral: true)
                self?.selectCustomDayRow.hidden = Condition(booleanLiteral: true)
            }
            
            customTypeRow.evaluateHidden()
            self?.selectCustomDayRow.evaluateHidden()
        }
        
        form +++ selectTypeRow
            +++ customTypeRow
            +++ selectCustomDayRow
        
        RepeatType.allStringValues.forEach { option in
            selectTypeRow <<< ImageCheckRow<String>(option) { listRow in
                listRow.title = option
                listRow.tag = option
                listRow.selectableValue = option
                listRow.value = nil
                listRow.cell.backgroundColor = UIColor.AppColor.selectedColor
                listRow.cell.textLabel?.textColor = UIColor.AppColor.Gray.Dark
            }
        }
        
        if let repeatTypeSelectedRow = form.rowBy(tag: row.value?.type?.getDescription() ?? "") as? ImageCheckRow<String> {
            repeatTypeSelectedRow.didSelect()
        }
        
        CustomDaysOfWeek.allStringValues.forEach { option in
            selectCustomDayRow <<< ImageCheckRow<String>(option) { listRow in
                listRow.title = option
                listRow.tag = option
                listRow.selectableValue = option
                listRow.value = nil
                listRow.cell.backgroundColor = UIColor.AppColor.selectedColor
                listRow.cell.textLabel?.textColor = UIColor.AppColor.Gray.Dark
            }
        }
        
        row.value?.customDays?.map{$0.getDescription()}.forEach { [weak self] in
            guard let strongSelf = self else {
                return
            }
            if let listRow = strongSelf.form.rowBy(tag: $0) as? ImageCheckRow<String> {
                listRow.didSelect()
            }
        }
    }
}
