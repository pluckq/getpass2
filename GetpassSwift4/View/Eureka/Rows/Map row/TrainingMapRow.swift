
//
//  TrainingMapRow.swift
//  Getpass
//
//  Created by vadim vitvickiy on 05/10/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Eureka

final class TrainingMapCell: PushSelectorCell<TrainingLocation> {
    
    @IBOutlet weak var valueLabel: UILabel!
    
    public override func setup() {
        super.setup()
        accessoryType = .disclosureIndicator
        backgroundColor = UIColor.AppColor.Gray.Light
        height = {eurekaCellHeight}
        layoutMargins = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        layoutMargins.left = eurekaMargin
        layoutMargins.right = eurekaMargin
        preservesSuperviewLayoutMargins = true
        contentView.preservesSuperviewLayoutMargins = true
        detailTextLabel?.textColor = UIColor.AppColor.Gray.Dark
        textLabel?.textColor = .black
    }
    
    public override func update() {
        super.update()
        if let name = row.value?.name {
            valueLabel.text = name
        }
    }
}

final class TrainingMapRow<Cell: TrainingMapCell>: OptionsRow<Cell>, PresenterRowType {
    
    var presentationMode: PresentationMode<TrainingCreateMapVC>?
    var onPresentCallback: ((FormViewController, TrainingCreateMapVC) -> Void)?
    typealias PresentedControllerType = TrainingCreateMapVC
    
    required public init(tag: String?) {
        super.init(tag: tag)
        
        presentationMode = .show(
            controllerProvider: ControllerProvider<TrainingCreateMapVC>.callback {
                return TrainingCreateMapVC.storyboardInstance()
            },
            onDismiss: { vc in vc.navigationController?.popViewController(animated: true) })
        
        displayValueFor = { [weak self] location in
            if let title = location?.name {
                return title
            } else {
                return self?.noValueDisplayText
            }
        }
    }
    
//    /**
//     Extends `didSelect` method
//     */
//    open override func customDidSelect() {
//        super.customDidSelect()
//        guard let presentationMode = presentationMode, !isDisabled else { return }
//        if let controller = presentationMode.makeController() {
//            controller.row = self
//            controller.title = selectorTitle ?? controller.title
//            onPresentCallback?(cell.formViewController()!, controller)
//            presentationMode.present(controller, row: self, presentingController: self.cell.formViewController()!)
//        } else {
//            presentationMode.present(nil, row: self, presentingController: self.cell.formViewController()!)
//        }
//    }
//
//    /**
//     Prepares the pushed row setting its title and completion callback.
//     */
//    open override func prepare(for segue: UIStoryboardSegue) {
//        super.prepare(for: segue)
//        guard let rowVC = segue.destination as? PresenterRow else { return }
//        rowVC.title = selectorTitle ?? rowVC.title
//        rowVC.onDismissCallback = presentationMode?.onDismissCallback ?? rowVC.onDismissCallback
//        onPresentCallback?(cell.formViewController()!, rowVC)
//        rowVC.row = self
//    }
}
