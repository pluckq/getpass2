//
//  TrainingMapVC.swift
//  Getpass
//
//  Created by vadim vitvickiy on 26/09/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit
import Eureka
import MapKit
import RxSwift

class TrainingCreateMapVC: UIViewController, MKMapViewDelegate, TypedRowControllerType {
    
    var row: RowOf<TrainingLocation>!
    var onDismissCallback : ((UIViewController) -> ())?

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var txtPlace: UITextField!
    @IBOutlet weak var txtDescription: UITextField!
    @IBOutlet weak var map: MKMapView!
    
    var selectedLocation: CLLocationCoordinate2D?
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        binds()
    }
    
    func configure() {
        title = "Выбор места"
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        map.delegate = self
        map.showsUserLocation = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(addPin(gestureRecognizer:)))
        map.addGestureRecognizer(tapGesture)
    }
    
    func binds() {
//        navigationItem.rightBarButtonItem = AppAppearance.navigationButton(title: "Готово")
        navigationItem.rightBarButtonItem?.tintColor = .white
        navigationController?.view.tintColor = .white
        navigationItem.rightBarButtonItem?.rx.tap.bind { [weak self] in
            self?.saveLocation()
        }.disposed(by: rx.disposeBag)
        
        searchBar.rx.searchButtonClicked.asObservable().bind { [weak self] in
            self?.view.endEditing(true)
            let text = self?.searchBar.text
            if  !text.isNilOrEmpty {
                self?.search(with: text!)
            }
        }.disposed(by: rx.disposeBag)
    }
    
    func saveLocation() {
        guard let name = txtPlace.text, let description = txtDescription.text, let location = selectedLocation, !txtPlace.text.isNilOrEmpty && !txtDescription.text.isNilOrEmpty else {
            showAlert(title: "Ошибка", message: "Заполните поля названия и описания места и выберите место")
            return
        }
        
        let trainingCoordinats = TrainingLocation(name: name, description: description, lat: location.latitude, lng: location.longitude)
        row.value = trainingCoordinats
        
        onDismissCallback!(self)
        
        DispatchQueue.main.async { [weak self] in
            self?.row.updateCell()
        }
    }
    
    func search(with text: String) {
        let request = MKLocalSearchRequest()
        request.naturalLanguageQuery = text
        request.region = map.region
        let search = MKLocalSearch(request: request)
        search.start(completionHandler: { [weak self] (response, error) in
            guard let response = response else {
                return
            }
            if let location = response.mapItems.first?.placemark.location {
                self?.centerMapOnLocation(location: location)
            } else {
                if let userLocation = self?.map.userLocation.location {
                    self?.centerMapOnLocation(location: userLocation)
                }
            }
        })
    }
    
    @objc func addPin(gestureRecognizer: UIGestureRecognizer) {
        
        view.endEditing(true)
        
        map.removeAnnotations(map.annotations)
        
        let touchPoint = gestureRecognizer.location(in: map)
        let newCoordinates = map.convert(touchPoint, toCoordinateFrom: map)
        let annotation = MKPointAnnotation()
        annotation.coordinate = newCoordinates
        map.addAnnotation(annotation)
        selectedLocation = newCoordinates
    }
    
    let regionRadius: CLLocationDistance = 1000
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius, regionRadius)
        map.setRegion(coordinateRegion, animated: true)
    }
    
    var didLocate = false
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if !didLocate {
            centerMapOnLocation(location: userLocation.location!)
            didLocate = true
        }
    }
}
