//
//  ImageCheckRow.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Eureka

public final class ImageCheckRow<T: Equatable>: Row<ImageCheckCell<T>>, SelectableRowType, RowType {
    public var selectableValue: T?
    required public init(tag: String?) {
        super.init(tag: tag)
        displayValueFor = nil
    }
}

public class ImageCheckCell<T: Equatable> : Cell<T>, CellType {
    
    required public init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        tintColor = UIColor.AppColor.Orange
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy public var trueImage: UIImage = {
        return #imageLiteral(resourceName: "check")
    }()
    
    lazy public var falseImage: UIImage = {
        return #imageLiteral(resourceName: "check")
    }()
    
    public override func update() {
        super.update()
        accessoryType = row.value != nil ? .checkmark : .none
    }
    
    public override func setup() {
        super.setup()
    }
    
    public override func didSelect() {
        row.updateCell()
        row.select()
        row.deselect()
    }
}

