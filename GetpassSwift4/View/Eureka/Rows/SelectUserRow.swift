//
//  TeamCreateSelectCapitanVC.swift
//  Getpass
//
//  Created by vadim vitvickiy on 06.07.17.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit
import Eureka
import RxSwift

final class UserInfoCell: PushSelectorCell<TeamMember> {

//    @IBOutlet weak var userView: UserView!
    
    public override func setup() {
        super.setup()
        setupCell()
    }
    
    public override func update() {
        super.update()
        setupCell()
    }
    
    func setupCell() {
//        if let user = row.value {
//            userView.configure(teamMember: user)
//        } else {
//            userView.configureEmptyRow(text: "Выбрать капитана")
//        }
    }
}

final class SelectUserPushRow<Cell: UserInfoCell>: OptionsRow<Cell>, PresenterRowType {
    
    typealias PresentedControllerType = TeamCreateSelectCapitanVC<User>
    var presentationMode: PresentationMode<TeamCreateSelectCapitanVC<User>>?
    var onPresentCallback: ((FormViewController, TeamCreateSelectCapitanVC<User>) -> Void)?
    
    required public init(tag: String?) {
        super.init(tag: tag)
        
        presentationMode = .show(
            controllerProvider: ControllerProvider<TeamCreateSelectCapitanVC<User>>.callback {
                return TeamCreateSelectCapitanVC<User>()
            },
            onDismiss: { vc in vc.navigationController?.popViewController(animated: true) })
        
        displayValueFor = { user in
            if let teamMember = user {
                return teamMember.user?.name
            } else {
                return nil
            }
        }
    }
    
//    public required init(tag: String?) {
//        super.init(tag: tag)
//        cellProvider = CellProvider<UserInfoCell>(nibName: UserInfoCell.className)
//        presentationMode = .show(controllerProvider: ControllerProvider.callback {
//            return TeamCreateSelectCapitanVC<T>()
//        }, onDismiss: { vc in
//            _ = vc.navigationController?.popViewController(animated: true)
//        })
//    }
}

class TeamCreateSelectCapitanVC<T: Equatable>: UIViewController, TypedRowControllerType {
    
    public var row: RowOf<TeamMember>!
    public var onDismissCallback: ((UIViewController) -> ())?
    
    let users = Variable<[TeamMember]>([])
    
    override func viewDidLoad() {
        
        title = "Выбрать капитана команды"
        
//        let tableView = UITableView(frame: view.bounds)
//        tableView.rowHeight = UserCell.cellHeigt
//        tableView.estimatedRowHeight = UserCell.cellHeigt
//        tableView.register(UINib(nibName: UserCell.className, bundle: nil), forCellReuseIdentifier: UserCell.className)
//        tableView.separatorStyle = .none
//        view.addSubview(tableView)
//
//        let options = row.dataProvider?.arrayData
//
//        if var newOptions = options {
//            newOptions.insert(TeamMember(), at: 0)
//            users.value = newOptions
//        }
//
//        users.asObservable().bind(to: tableView.rx.items(cellIdentifier: UserCell.className, cellType: UserCell.self)) { (index, user: TeamMember, cell: UserCell) in
//            if index == 0 {
//                cell.userView.configureEmptyRow(text: "Не выбран")
//            } else {
//                cell.userView.configure(teamMember: user)
//            }
//        }.disposed(by: rx.disposeBag)
//
//        tableView.rx.itemSelected.asObservable().bind { [weak self] ip in
//            var user: TeamMember?
//            if ip.row == 0 {
//                user = nil
//            } else {
//                user = self?.users.value[ip.row]
//            }
//            self?.row.value = user
//            self?.row.updateCell()
//            self?.navigationController?.popViewController()
//        }.disposed(by: rx.disposeBag)
    }
}
