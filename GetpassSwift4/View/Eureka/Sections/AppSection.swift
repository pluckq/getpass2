//
//  AppSection.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Eureka

class AppSection: Section {

    var myHeaderString: String? {
        didSet {
            let view = header?.viewForSection(self, type: .header)
            view?.backgroundColor = .white
            view?.setNeedsLayout()
        }
    }
    
    override init(_ header: String, _ initializer: (Section) -> ()) {
        myHeaderString = header
        super.init(header, initializer)
        var header = HeaderFooterView<AutoSizeLabelView>(.class)
        header.height = { UITableViewAutomaticDimension }
        header.onSetupView = { v, s in
            v.label.text = (s as? AppSection)?.myHeaderString
            v.label.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.medium)
        }
        self.header = header
    }
    
    required override init( _ initializer: (Section) -> ()) {
        var header = HeaderFooterView<AutoSizeLabelView>(.class)
        header.height = { UITableViewAutomaticDimension }
        header.onSetupView = { v, s in
            v.label.text = (s as? AppSection)?.myHeaderString
            v.label.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.medium)
        }
        
        super.init(initializer)
    }
    
    required init() {
        var header = HeaderFooterView<AutoSizeLabelView>(.class)
        header.height = { UITableViewAutomaticDimension }
        header.onSetupView = { v, s in
            v.label.text = (s as? AppSection)?.myHeaderString
            v.label.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.medium)
        }
        super.init()
    }
}
