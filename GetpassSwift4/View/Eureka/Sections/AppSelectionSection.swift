//
//  AppSelectionSection.swift
//  GetpassSwift4
//
//  Created by vadim vitvickiy on 09/11/2017.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import Eureka

class AppSelectionSection: SelectableSection<ImageCheckRow<String>> {

    var myHeaderString: String? {
        didSet {
            let view = header?.viewForSection(self, type: .header)
            view?.backgroundColor = .white
            view?.setNeedsLayout()
        }
    }
    
    override init(_ header: String, selectionType: SelectionType, _ initializer: (SelectableSection<ImageCheckRow<String>>) -> Void) {
        myHeaderString = header
        super.init(header, selectionType: selectionType, initializer)
        var header = HeaderFooterView<StaticLabelView>(.class)
        header.height = { UITableViewAutomaticDimension }
        header.onSetupView = { v, s in
            v.label.text = (s as? AppSelectionSection)?.myHeaderString
            v.label.font = .systemFont(ofSize: 15, weight: UIFont.Weight.medium)
        }
        self.header = header
    }
    
    required init( _ initializer: (Section) -> ()) {
        var header = HeaderFooterView<StaticLabelView>(.class)
        header.height = { UITableViewAutomaticDimension }
        header.onSetupView = { v, s in
            v.label.text = (s as? AppSelectionSection)?.myHeaderString
            v.label.font = .systemFont(ofSize: 15, weight: UIFont.Weight.medium)
        }
        
        super.init(initializer)
    }
    
    required init() {
        var header = HeaderFooterView<StaticLabelView>(.class)
        header.height = { UITableViewAutomaticDimension }
        header.onSetupView = { v, s in
            v.label.text = (s as? AppSelectionSection)?.myHeaderString
            v.label.font = .systemFont(ofSize: 15, weight: UIFont.Weight.medium)
        }
        super.init()
    }
    
}
