//
//  HighlitableCell.swift
//  Getpass
//
//  Created by vadim vitvickiy on 07.07.17.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift

class BaseAppCell: UITableViewCell {

    let disposeBag = DisposeBag()
    
    private(set) var bag = DisposeBag()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.backgroundColor = highlighted ? UIColor.AppColor.selectedColor : .white
        }
    }
}



